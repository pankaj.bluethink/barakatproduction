<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_TimeSlotDelivery
 * @author Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\TimeSlotDelivery\Plugin\Magento\Checkout\Model;

use Magento\Framework\Session\SessionManager;
use Magento\Framework\Exception\CouldNotSaveException;

class ShippingInformationManagement
{
    /**
     * @var SessionManager
     */
    protected $_coreSession;
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager = null;

     /**
      * @var \Webkul\MpTimeDelivery\Helper\Data
      */
    protected $_helper;

    /**
     * @param SessionManager                            $coreSession
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Quote\Model\QuoteRepository      $quoteRepository
     * @param \Webkul\MpTimeDelivery\Helper\Data        $helper
     */
    public function __construct(
        SessionManager $coreSession,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Webkul\TimeSlotDelivery\Helper\Data $helper
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->_coreSession = $coreSession;
        $this->_objectManager = $objectManager;
        $this->_helper = $helper;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        
        if ($this->_helper->getConfigData('enable')) {
            $extAttributes = $addressInformation->getExtensionAttributes();
            if (!$extAttributes) {
                return [$cartId, $addressInformation];
            }
            $slotData = $extAttributes->getSlotData();
            
            $quote = $this->quoteRepository->getActive($cartId);
            
            $sellerId = 0;
            if ($slotData instanceof \Webkul\TimeSlotDelivery\Model\SelectedSlot && $quote->getId()) {
                if ($this->_helper->isSlotAvailable($slotData->getSlotId(), $slotData->getDate())) {
                    $quote->setOrderDeliveryDate($slotData->getDate());
                    $quote->setOrderDeliveryTime($slotData->getSlotTime());
                    $quote->save();
                    if ($this->_coreSession->getSlotInfo()) {
                        $this->_coreSession->unsSlotInfo();
                        $this->_coreSession->setSlotInfo($slotData);
                    } else {
                        $this->_coreSession->setSlotInfo($slotData);
                    }
                } else {
                    throw new CouldNotSaveException(
                        __('Selected slot full, please select another available slot.')
                    );
                }
            }
        }
        return [$cartId, $addressInformation];
    }
}
