<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_TimeSlotDelivery
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\TimeSlotDelivery\Plugin\Magento\Checkout\Model;

use Magento\Framework\Session\SessionManager;
use Magento\Framework\Exception\CouldNotSaveException;

class GuestPaymentInformationManagement
{
    /**
     * @var SessionManager
     */
    protected $_coreSession;
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager = null;

     /**
      * @var \Webkul\MpTimeDelivery\Helper\Data
      */
    protected $_helper;

    /**
     * @param SessionManager                            $coreSession
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Quote\Model\QuoteRepository      $quoteRepository
     * @param \Webkul\MpTimeDelivery\Helper\Data        $helper
     */
    public function __construct(
        SessionManager $coreSession,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Webkul\TimeSlotDelivery\Helper\Data $helper
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->_coreSession = $coreSession;
        $this->_objectManager = $objectManager;
        $this->_helper = $helper;
    }

    public function beforeSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Model\GuestPaymentInformationManagement $subject,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        if ($this->_helper->getConfigData('enable')) {
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            $quote = $this->quoteFactory->create()->load($quoteIdMask->getQuoteId());
            $extAttributes = $paymentMethod->getExtensionAttributes();
            if (!$extAttributes) {
                return [$cartId, $email, $paymentMethod, $billingAddress];
            }
            $slotData = $extAttributes->getSlotData();

            if ($slotData instanceof \Webkul\TimeSlotDelivery\Model\SelectedSlot) {
                if ($this->_helper->isSlotAvailable($slotData->getSlotId(), $slotData->getDate())) {
                    $quote->setOrderDeliveryDate($slotData->getDate());
                    $quote->setOrderDeliveryTime($slotData->getSlotTime());
                    $quote->save();
                    if ($this->_coreSession->getSlotInfo()) {
                        $this->_coreSession->unsSlotInfo();
                        $this->_coreSession->setSlotInfo($slotData);
                    } else {
                        $this->_coreSession->setSlotInfo($slotData);
                    }
                } else {
                    throw new CouldNotSaveException(
                        __('Selected slot full, please select another available slot.')
                    );
                }
            } elseif (is_object($this->_coreSession->getSlotInfo())) {
                $slotData = $this->_coreSession->getSlotInfo();
                if ($this->_helper->isSlotAvailable($slotData->getSlotId(), $slotData->getDate())) {
                } else {
                    throw new CouldNotSaveException(
                        __('Selected slot full, please select another available slot.')
                    );
                }
            }
        }
        return [$cartId, $email, $paymentMethod, $billingAddress];
    }
}
