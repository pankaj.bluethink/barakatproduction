var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Webkul_TimeSlotDelivery/js/view/shipping': true
            },
            'Magento_Checkout/js/view/payment/default': {
                'Webkul_TimeSlotDelivery/js/view/payment/default': true
            }
        }
    },
    "map": {
        "*": {
            'Magento_Checkout/js/model/shipping-save-processor/default': 'Webkul_TimeSlotDelivery/js/model/shipping-save-processor/default'
        }
    }
};