/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_TimeSlotDelivery
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

define(
    [
    'jquery',
    'Webkul_TimeSlotDelivery/js/view/admin-time-slots',
    'Magento_Customer/js/customer-data'
    ],
    function ($, timeSlots, customerData) {
    'use strict';

    return function (Default) {
        return Default.extend({
            getData: function () {
                var data = this._super();
                data['extension_attributes'] = {
                    slot_data: this.getSlotInfo(),
                }
                return data;
            },
            getSlotInfo: function () {
                if ($.isEmptyObject(timeSlots().selectedSlots())) {
                    var slots = customerData.get("selected-slots");
                    return slots();
                }
                return timeSlots().selectedSlots();
            }
        });
    }
    }
);
