define(['jquery'], function ($) {
    'use strict';

    return function (payload) {
        payload.addressInformation['extension_attributes'] = {
            'slot_data': $('[name="slot_data"]').val()
        };

        return payload;
    };
});