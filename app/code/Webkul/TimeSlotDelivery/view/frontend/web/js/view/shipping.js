/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Webkul_TimeSlotDelivery/js/view/admin-time-slots',
    'mage/translate'
], function (
    adminTimeSlots,
    $t
) {
        'use strict';

        return function (Shipping) {
            return Shipping.extend({
                validateShippingInformation: function () {
                    if (adminTimeSlots().slotCount() == 0 && adminTimeSlots().slotEnabled()) {
                        this.errorValidationMessage($t('Please select a delivery time slot.'));
                        return false;
                    }
                    return this._super();
                }
            });
        }
    });
