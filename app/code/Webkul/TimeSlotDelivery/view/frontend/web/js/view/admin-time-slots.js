/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_MpTimeDelivery
 * @author Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data',
    'underscore',
    "Webkul_TimeSlotDelivery/js/view/moment-with-locales",
    "jquery/ui",
    "mage/calendar",
], function ($, ko, Component, quote, customerData, _, moment) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'Webkul_TimeSlotDelivery/view/admin-time-slots'
        },
        slotCount: ko.observable(0),
        slotEnabled: ko.observable(window.checkoutConfig.slotEnabled),
        selectedSlots: ko.observableArray([]),
        initialize: function () {
            this._super();
            var self = this;
            this.datesArray = [];
            this.allowedDays = window.checkoutConfig.allowed_days;
            this.isEnabled = window.checkoutConfig.slotEnabled;
            this.slotData = window.checkoutConfig.slotData;
            this.startDate = window.checkoutConfig.start_date;
            this.slots = ko.observableArray([]);
            this.sortedSlots = ko.observableArray([]);
            this.selectValue = ko.observable(0);
            this.isChecked = ko.observable(false);
            this.slotForSelect = ko.observableArray([{ slot: 'Please Select Date', disable:true, date: self.startDate, slot_id:'caption'}]);
            this.currentDate = this.startDate;
            this.maxDays = window.checkoutConfig.max_days;
            this.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            self.slots.push(this.slotData);
            this.locale = window.checkoutConfig.locale;
            $.each(this.slotData['slots'], function ( index, value ) {
              self.datesArray.push(index);
            });
        },
        addDatePicker: function (startDate) {
          var self = this;
          $("#dateForSlot").calendar({
                  'dateFormat':'yy-mm-dd',
                  'changeMonth': true,
                  'changeYear': true,
                  beforeShowDay : function (date) {
                    var formattedDate = $.datepicker.formatDate('yy-mm-dd', date);
                    var dayStatus = (self.datesArray.indexOf(formattedDate) != -1) && (self.checkDay(formattedDate,startDate));
                    return [dayStatus, dayStatus?'wk-green':'wk-red'];
                  },
                  onSelect : function (date) {
                    self.slotCount(0);
                    self.slotForSelect([{ slot: 'Please Select Slot', disable:false, date: self.getDate(date), slot_id:'topCaption'}]);
                    $.each(self.slotData['slots'][date], function ( index, value ) {
                      value['date'] = self.getDate(date);
                      value['disable'] = !value['is_available'] || !self.checkTime(value['slot'],date);
                      self.slotForSelect.push(value);
                    });
                  }
              });
        },
        getSlotData: function () {
            return this.slots;
        },
        // getSlotStorage: function (data) {
        //     if ($.isEmptyObject(this.selectedSlots())) {
        //         var slots = customerData.get("selected-slots");
        //         if (!$.isEmptyObject(slots)) {
        //             //this.slotCount(1);
        //         }
        //     }
        //
        // },
        // getSortedSlots: function (data) {
        //     var ordered = {};
        //     Object.keys(data).sort().forEach(function (key) {
        //         ordered[key] = data[key];
        //     });
        //     return ordered;
        // },
        getDate: function (cday, translate) {
          //console.log(cday);
            var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
 return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
            var isiPhone = navigator.userAgent.toLowerCase().indexOf("iphone");
            var locale = this.locale;
            var cDate = new Date(cday);
            if (isSafari || isiPhone) {
                var dt = cday.split('-');
                var dat = dt[1]+'/'+dt[2]+'/'+dt[0];
                var cDate = new Date(dat);
            }
            var cDay = cDate.getDay();
            var returnDate;
            var check = 0;
            for (var i = 0; i <= this.maxDays; i++) {
                var nDate = new Date(this.currentDate);
                if (isSafari || isiPhone) {
                    var dt1 = this.currentDate.split(' ');
                    var dt = dt1[0].split('-');
                    var dat = dt[1]+'/'+dt[2]+'/'+dt[0]+' '+dt1[1];
                    var nDate = new Date(dat);
                }
                nDate.setDate(nDate.getDate() + check);
                var day = nDate.getDate();
                var month = nDate.getMonth() + 1;
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var d = new Date(nDate.getFullYear() + "-" + month + "-" + day);
                if (isSafari || isiPhone) {
                    var d = new Date(month + "/" + day+ "/" +nDate.getFullYear());
                }
                var n = d.getDay();
                if (n == cDay) {
                    if (window.checkoutConfig.localeExists || window.checkoutConfig.locale != '') {
                        if (translate) {
                            var d = moment(cDate);
                            var newDate = d.locale(locale).format('dddd, D MMMM, YYYY');
                            returnDate = newDate;
                        } else {
                            returnDate = $.datepicker.formatDate('DD, d MM, yy', cDate);
                        }
                        // returnDate = $.datepicker.formatDate('DD, d MM, yy', cDate, {
                        //     dayNamesShort: $.datepicker.regional[locale].dayNamesShort,
                        //     dayNames: $.datepicker.regional[locale].dayNames,
                        //     monthNamesShort: $.datepicker.regional[locale].monthNamesShort,
                        //     monthNames: $.datepicker.regional[locale].monthNames
                        // });
                    } else {
                        returnDate = $.datepicker.formatDate('DD, d MM, yy', cDate);
                    }
                    //returnDate = cday +'/'+this.weekdays[n];
                    break;
                }
                check++;
            }

            //this.currentDate = nDate.getFullYear() + "-" + month + "-" + day;
            return returnDate;
        },
        checkDay: function (day, sellerStart) {
            var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
 return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
            var isiPhone = navigator.userAgent.toLowerCase().indexOf("iphone");
            var result = sellerStart.split(' ');
            if (sellerStart) {
                if (isSafari || isiPhone) {
                    var dt = result[0];
                    var dt = dt.split('-');
                    var dat = dt[1]+'/'+dt[2]+'/'+dt[0];
                    var d = new Date(dat);
                } else {
                    var d = new Date(result[0]);
                }
            } else {
                if (isSafari || isiPhone) {
                    var dt = startDate.split('-');
                    var dat = dt[1]+'/'+dt[2]+'/'+dt[0];
                    var d = new Date(dat);
                } else {
                    var d = new Date(this.startDate);
                }
            }
            if (isSafari || isiPhone) {
                var dt = day.split('-');
                var dat = dt[1]+'/'+dt[2]+'/'+dt[0];
                var requestedDay = new Date(dat);
            } else {
                var requestedDay = new Date(day);
            }
            if (requestedDay >= d) {
                return true;
            }
            return false;
        },
        checkTime: function (time, date) {
            var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
 return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
            var isiPhone = navigator.userAgent.toLowerCase().indexOf("iphone");
            var result = time.split('-');
            var currentTime = new Date(this.startDate).getTime();
            if (isSafari || isiPhone) {
                var dt1 = this.startDate.split(' ');
                var dt = dt1[0].split('-');
                var dat = dt[1]+'/'+dt[2]+'/'+dt[0]+' '+dt1[1];
                var currentTime = new Date(dat).getTime();
            }
            var slotTime1 = new Date(this.convertDate(date + " " + result[1].replace(' ', ''))).getTime();
            var slotTime0 = new Date(this.convertDate(date + " " + result[0].replace(' ', ''))).getTime();
            if (currentTime > slotTime0 && currentTime < slotTime1) {
                return false;
            }
            if (currentTime < slotTime1) {
                return true;
            }
            return false;
        },
        convertDate: function (date) {
            var isiPhone = navigator.userAgent.toLowerCase().indexOf("iphone");
            var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) {
 return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
            // # valid js Date and time object format (YYYY-MM-DDTHH:MM:SS)
            var dateTimeParts = date.split(' ');

            // # this assumes time format has NO SPACE between time and AM/PM marks.
            if (dateTimeParts[1].indexOf(' ') == -1 && dateTimeParts[2] === undefined) {
                var theTime = dateTimeParts[1];

                // # strip out all except numbers and colon
                var ampm = theTime.replace(/[0-9:]/g, '');

                // # strip out all except letters (for AM/PM)
                var time = theTime.replace(/[[^a-zA-Z]/g, '');
                if (ampm == 'PM') {
                    time = time.split(':');
                    if (time[0] == 12) {
                        time = parseInt(time[0]) + ':' + time[1] + ':00';
                    } else {
                        time = parseInt(time[0]) + 12 + ':' + time[1] + ':00';
                    }
                } else { // if AM
                    time = time.split(':');
                    if (time[0] < 10) {
                        time = '0' + time[0] + ':' + time[1] + ':00';
                    } else {
                        time = time[0] + ':' + time[1] + ':00';
                    }
                }
            }
            var value = dateTimeParts[0] + 'T' + time;
            var date = new Date(dateTimeParts[0] + 'T' + time);
            if (isSafari || isiPhone) {
                var dt = dateTimeParts[0].split('-');
                var dat = dt[1]+'/'+dt[2]+'/'+dt[0] + ' ' + time;
                var date = new Date(dat);
            }

            return date;
        },
        // checkIsSlotsAvailable: function () {
        //
        // },
        // refreshVars: function () {
        //     this.currentDate = this.startDate;
        // },
        // generateClass: function (name) {
        //     return name.replace(/\s+/g, '-').toLowerCase();
        // },
        // generateId: function (date, id) {
        //     var data = this.getDate(date).replace(/\s+/g, '-');
        //     return data.replace(',', '_').toLowerCase() + '_' + id;
        // },
        // isSelected: function (model, seller, data, event) {
        //     if ($(event.currentTarget).hasClass('disabled') == false) {
        //         var elem = event.currentTarget;
        //         $('.slot').removeClass('selected');
        //         $(event.currentTarget).addClass('selected');
        //     }
        //
        //
        // },
        selectTimeSlot: function () {
            var self = this;
            var index = self.selectValue();
            self.slotCount(0);
            $(".selected-slots").remove();
            if (index != 0) {
                var selected = {
                    'slot_time': self.slotForSelect()[index]['slot'],
                    'date': self.slotForSelect()[index]['date'],
                    'slot_id': self.slotForSelect()[index]['slot_id']
                };
                self.selectedSlots(selected);
                self.slotCount(1);
                customerData.set("selected-slots", self.selectedSlots());
                $('#co-shipping-method-form').append("<input class='selected-slots' type='hidden' name='slot_data' value='" + JSON.stringify(self.selectedSlots()) + "'/>");
            }
            return true;
        }
    })
});
