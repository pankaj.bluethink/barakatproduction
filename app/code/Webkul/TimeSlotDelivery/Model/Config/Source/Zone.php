<?php
/**
 * Webkul Software
 *
 * @category Bluethink
 * @package Bluethink_Webkul_TimeSlotDelivery
 * @author Bluethink
 * @copyright Copyright (c) 2010-2017 Bluethink Software Private Limited (https://webkul.com)
 * @license Bluethink
 */
namespace Webkul\TimeSlotDelivery\Model\Config\Source;

/**
 *
 */
class Zone implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $zone = [
            ['value' => 'Airport Road',       'label' => __('Airport Road')],
            ['value' => 'AL  Matar',       'label' => __('AL  Matar')],
            ['value' => 'Al Aman',       'label' => __('Al Aman')],
            ['value' => 'Al Bahya',       'label' => __('Al Bahya')],
            ['value' => 'Al Bandar',       'label' => __('Al Bandar')],

           
        ];

        return $zone;
    }
}
