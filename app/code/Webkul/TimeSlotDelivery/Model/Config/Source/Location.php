<?php
/**
 * Webkul Software
 *
 * @category Bluethink
 * @package Bluethink_Webkul_TimeSlotDelivery
 * @author Bluethink
 * @copyright Copyright (c) 2010-2017 Bluethink Software Private Limited (https://webkul.com)
 * @license Bluethink
 */
namespace Webkul\TimeSlotDelivery\Model\Config\Source;

/**
 *
 */
class Location implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $location = [
            ['value' => 'Abu Dhabi',       'label' => __('Abu Dhabi')],
            ['value' => 'Dubai',       'label' => __('Dubai')],
            ['value' => 'Sharjah',       'label' => __('Sharjah')],
            
        ];

        return $location;
    }
}
