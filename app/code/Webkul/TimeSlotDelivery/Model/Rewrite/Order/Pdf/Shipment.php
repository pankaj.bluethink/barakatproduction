<?php

namespace Webkul\TimeSlotDelivery\Model\Rewrite\Order\Pdf;

class Shipment extends \Magento\Sales\Model\Order\Pdf\Shipment
{
    public function getPdf($shipments = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);
        foreach ($shipments as $shipment) {
            if ($shipment->getStoreId()) {
                $this->_localeResolver->emulate($shipment->getStoreId());
                $this->_storeManager->setCurrentStore($shipment->getStoreId());
            }
            $page = $this->newPage();
            $order = $shipment->getOrder();
            /* Add image */
            $this->insertLogo($page, $shipment->getStore());
            /* Add address */
            $this->insertAddress($page, $shipment->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $shipment,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_SHIPMENT_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );
            /* Add document text and number */
            $this->insertDocumentNumber($page, __('Packing Slip # ') . $shipment->getIncrementId());
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($shipment->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
            if ($order->getOrderDeliveryDate() && $order->getOrderDeliveryTime()) {
                $this->addDeliveryTimeSlot($page, $order);
            }
        }

        $this->_afterGetPdf();
        if ($shipment->getStoreId()) {
            $this->_localeResolver->revert();
        }
        return $pdf;
    }

    public function addDeliveryTimeSlot(\Zend_Pdf_Page $page, $order)
    {
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 45);
        $this->y -= 10;
        $page->setFillColor(new \Zend_Pdf_Color_RGB(0.1, 0.1, 0.1));
        $lineBlock = ['lines' => [], 'height' => 15];

        $lineBlock['lines'][] = [
            [
                'text' => __('Delivery Time Slot'),
                'feed' => '35'
            ]
        ];

        if ($order->getOrderDeliveryDate()) {
            $lineBlock['lines'][] = [
                [
                    'text' => __('Delivery Date:'),
                    'feed' => '35',
                    'font' => 'bold'
                ],
                [
                    'text' => $order->getOrderDeliveryDate(),
                    'feed' => '100'
                ]
            ];
        }

        if ($order->getOrderDeliveryTime()) {
            $lineBlock['lines'][] = [
                [
                    'text' => __('Delivery Time:'),
                    'feed' => '35',
                    'font' => 'bold'
                ],
                [
                    'text' => $order->getOrderDeliveryTime(),
                    'feed' => '100'
                ]
            ];
        }

        $page = $this->drawLineBlocks($page, [$lineBlock]);
    }
}
