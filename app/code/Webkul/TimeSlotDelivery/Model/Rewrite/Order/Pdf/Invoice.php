<?php

namespace Webkul\TimeSlotDelivery\Model\Rewrite\Order\Pdf;

class Invoice extends \Magento\Sales\Model\Order\Pdf\Invoice
{
    
    public function getPdf($invoices = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                $this->_localeResolver->emulate($invoice->getStoreId());
                $this->_storeManager->setCurrentStore($invoice->getStoreId());
            }
            $page = $this->newPage();
            $order = $invoice->getOrder();
            /* Add image */
            $this->insertLogo($page, $invoice->getStore());
            /* Add address */
            $this->insertAddress($page, $invoice->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $order,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );
            /* Add document text and number */
            $this->insertDocumentNumber($page, __('Invoice # ') . $invoice->getIncrementId());
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($invoice->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
            /* Add totals */
            $this->insertTotals($page, $invoice);
            if ($invoice->getStoreId()) {
                $this->_localeResolver->revert();
            }
        }
        
        if ($order->getOrderDeliveryDate() && $order->getOrderDeliveryTime()) {
            $this->addDeliveryTimeSlot($page, $order);
        }

        $this->_afterGetPdf();
        return $pdf;
    }

    /**
     * Add time slot on invoice pdf
     *
     * @param \Zend_Pdf_Page $page
     * @param \Magento\Sales\Model\Order $order
     * @return void
     */
    public function addDeliveryTimeSlot(\Zend_Pdf_Page $page, $order)
    {
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y - 45);
        $this->y -= 10;
        $page->setFillColor(new \Zend_Pdf_Color_RGB(0.1, 0.1, 0.1));
        $lineBlock = ['lines' => [], 'height' => 15];
        
        $lineBlock['lines'][] = [
            [
                'text' => __('Delivery Time Slot'),
                'feed' => '35'
            ]
        ];

        if ($order->getOrderDeliveryDate()) {
            $lineBlock['lines'][] = [
                [
                    'text' => __('Delivery Date:'),
                    'feed' => '35',
                    'font' => 'bold'
                ],
                [
                    'text' => $order->getOrderDeliveryDate(),
                    'feed' => '100'
                ]
            ];
        }

        if ($order->getOrderDeliveryTime()) {
            $lineBlock['lines'][] = [
                [
                    'text' => __('Delivery Time:'),
                    'feed' => '35',
                    'font' => 'bold'
                ],
                [
                    'text' => $order->getOrderDeliveryTime(),
                    'feed' => '100'
                ]
            ];
        }

        $page = $this->drawLineBlocks($page, [$lineBlock]);
    }
}
