<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_TimeSlotDelivery
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\TimeSlotDelivery\Model;

use Webkul\TimeSlotDelivery\Api\TimeSlotsManagementInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Webkul\TimeSlotDelivery\Model\ResourceModel\TimeSlots\CollectionFactory;

class TimeSlotsManagement implements TimeSlotsManagementInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;
    /**
     * @var \Webkul\TimeSlotDelivery\Api\Data\ResponseInterface
     */
    public $response;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var CollectionFactory
     */
    protected $_timeSlotCollection;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Webkul\TimeSlotDelivery\Model\OrderFactory
     */
    protected $slotsOrderFactory;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezone;
    
    /**
     *
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Webkul\TimeSlotDelivery\Api\Data\ResponseInterface $response
     * @param \Webkul\TimeSlotDelivery\Model\OrderFactory $slotsOrderFactory
     * @param CollectionFactory $timeSlotCollection
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\TimeSlotDelivery\Api\Data\ResponseInterface $response,
        \Webkul\TimeSlotDelivery\Model\OrderFactory $slotsOrderFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        CollectionFactory $timeSlotCollection
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->_objectManager = $objectManager;
        $this->_timeSlotCollection = $timeSlotCollection;
        $this->_date = $date;
        $this->response = $response;
        $this->slotsOrderFactory = $slotsOrderFactory;
        $this->_timezone = $timezone;
    }
    /**
     * @inheritDoc
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getDisplaySlots()
    {
        $store = $this->storeManager->getStore()->getStoreId();
        $allowedDays = $this->scopeConfig->getValue(self::XPATH_ALLOWED_DAY, ScopeInterface::SCOPE_STORE, $store);
        $processTime = $this->scopeConfig->getValue(self::XPATH_PROCESS_TIME, ScopeInterface::SCOPE_STORE, $store);
        $maxDays = $this->scopeConfig->getValue(self::XPATH_MAX_DAYS, ScopeInterface::SCOPE_STORE, $store);
        $message = $this->scopeConfig->getValue(self::XPATH_MESSAGE, ScopeInterface::SCOPE_STORE, $store);
        $isEnabled = (bool)$this->scopeConfig->getValue(self::ENABLED, ScopeInterface::SCOPE_STORE, $store);
        $locale = $this->scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE, $store);
        $result = [
            'error'=> false,
            'message' => __('Time slot not enabled, please contact to admin.'),
            'slots' => []
        ];
        if (!$isEnabled) {
            return $this->response->setItem($result);
        }

        if (!$processTime) {
            $processTime=0;
        }
       
        $date = strtotime("+".$processTime." day", strtotime(date('Y-m-d H:i:s')));
        $allowedDays = explode(',', $allowedDays);
        $date = $this->_date;

        $collection = $this->_timeSlotCollection->create()
            ->addFieldToFilter('status', ['eq' => 1]);
        $dateWiseSlots = [];

        $startDate = '';
        $startDate = date("Y-m-d", strtotime("+".$processTime." day", strtotime(date('Y-m-d'))));
        if ($collection->getSize()) {
            foreach ($collection as $slot) {
                if (!in_array($slot->getDeliveryDay(), $allowedDays)) {
                    continue;
                }
                $startTime = $date->gmtDate('g:i A', $slot->getStartTime());
                $endTime = $date->gmtDate('g:i A', $slot->getEndTime());
  
                for ($i=0; $i <= $maxDays; $i++) {
                    $d = strtotime("+".$i." day", strtotime(date('Y-m-d')));
                    if (ucfirst($slot->getDeliveryDay()) == date('l', $d)) {
                        $isAvailable = $this->checkAvailabilty($slot, $d);
                        if (strtotime(date('Y-m-d', $d)) >= strtotime($startDate)) {
                            $dateWiseSlots[date('Y-m-d', $d)][] = [
                                'id'   => $slot->getEntityId(),
                                'time'=> $startTime.'-'.$endTime,
                                'availability' => $this->getTotalAvailableSlots($slot, $d),
                                'enable'=>$isAvailable
                            ];
                        }
                    }
                }
            }
            $result['message'] = __('%1 slots available', $collection->getSize());
        } else {
            $result['error'] =  true;
            $result['message'] = __('No slots available');
        }
        
        ksort($dateWiseSlots);
        // Remove date if all slots are disabled.
        foreach ($dateWiseSlots as $key => $slots) {
            $isRemove = true;
            foreach ($slots as $slot) {
                if ($slot['enable']) {
                    $isRemove = false;
                }
            }
            if ($isRemove) {
                unset($dateWiseSlots[$key]);
            }
        }
        $result['slots'] = $dateWiseSlots;
        return $this->response->setItem($result);
    }

    /**
     *
     * @param \Webkul\TimeSlotDelivery\Model\TimeSlots $slot
     * @param string $date
     * @return void
     */
    private function checkAvailabilty($slot, $date)
    {
        $date = $this->_date->gmtDate(date('Y-m-d', $date));
        $collection = $this->slotsOrderFactory->create()
            ->getCollection()
            ->addFieldToFilter('slot_id', ['eq' => $slot->getEntityId()])
            ->addFieldToFilter('selected_date', ['eq' => $date]);
        if ($collection->getSize() >= $slot->getOrderCount()) {
            return false;
        }

        $currentDate = $this->_timezone->date()->format('Y-m-d');
        $currentTime = $this->_timezone->date()->format('H:i:s');
        if (strtotime($date) == strtotime($currentDate)) {
            if (strtotime($currentTime) > strtotime($slot->getStartTime()) && strtotime($currentTime) < strtotime($slot->getEndTime())) {
                return false;
            } elseif (strtotime($currentTime) > strtotime($slot->getEndTime())) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param \Webkul\TimeSlotDelivery\Model\TimeSlots $slot
     * @param string $date
     * @return int
     */
    public function getTotalAvailableSlots($slot, $date)
    {
        $date = $this->_date->gmtDate(date('Y-m-d', $date));
        $collection = $this->slotsOrderFactory->create()
            ->getCollection()
            ->addFieldToFilter('slot_id', ['eq' => $slot->getEntityId()])
            ->addFieldToFilter('selected_date', ['eq' => $date]);

        if ($collection->getSize()) {
            return (int) $slot->getOrderCount() - (int) $collection->getSize();
        }

        return (int) $slot->getOrderCount();
    }
}
