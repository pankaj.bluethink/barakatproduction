<?php
/**
 * api response.
 *
 * @category  Webkul_TimeSlotDelivery
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\TimeSlotDelivery\Model;

class Response extends \Magento\Framework\DataObject implements \Webkul\TimeSlotDelivery\Api\Data\ResponseInterface
{

    /**
     * prepare api response .
     *
     * @return \Webkul\TimeSlotDelivery\Api\Data\ResponseInterface
     */
    public function getResponse()
    {
        $data = $this->_data;
        return $data;
    }
}
