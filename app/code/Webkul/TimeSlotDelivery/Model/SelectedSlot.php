<?php
namespace Webkul\TimeSlotDelivery\Model;

class SelectedSlot extends \Magento\Framework\DataObject implements \Webkul\TimeSlotDelivery\Api\Data\SelectedSlotInterface
{
    /**
     * {@inheritdoc}
     */
    public function setSlotTime($slotTime)
    {
        return $this->setData(self::TIME, $slotTime);
    }

    /**
     * {@inheritdoc}
     */
    public function setDate($slotDate)
    {
        return $this->setData(self::DATE, $slotDate);
    }

    /**
     * {@inheritdoc}
     */
    public function setSlotId($slotId)
    {
        return $this->setData(self::SLOT_ID, $slotId);
    }

    /**
     * {@inheritdoc}
     */
    public function getSlotTime()
    {
        return $this->getData(self::TIME);
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return $this->getData(self::DATE);
    }

    /**
     * {@inheritdoc}
     */
    public function getSlotId()
    {
        return $this->getData(self::SLOT_ID);
    }
}
