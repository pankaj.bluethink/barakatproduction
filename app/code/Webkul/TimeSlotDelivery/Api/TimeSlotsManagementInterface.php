<?php
namespace Webkul\TimeSlotDelivery\Api;

/**
 * @api
 */
interface TimeSlotsManagementInterface
{
    const XPATH_ALLOWED_DAY     = 'time_slots_delivery/general/allowed_days';
    const XPATH_PROCESS_TIME    = 'time_slots_delivery/general/process_time';
    const XPATH_MAX_DAYS        = 'time_slots_delivery/general/maximum_days';
    const ENABLE                = 'time_slots_delivery/general/active';
    const XPATH_MESSAGE         = 'time_slots_delivery/general/message';
    const ENABLED               = 'time_slots_delivery/general/enable';
    
    /**
     * get all display slots on checkout
     * @return Webkul\TimeSlotDelivery\Api\Data\ResponseInterface
     */
    public function getDisplaySlots();
}
