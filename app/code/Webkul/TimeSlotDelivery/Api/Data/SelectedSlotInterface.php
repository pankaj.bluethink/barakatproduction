<?php
namespace Webkul\TimeSlotDelivery\Api\Data;

interface SelectedSlotInterface
{
    const TIME = 'slot_time';
    const DATE = 'date';
    const SLOT_ID = 'slot_id';
    /**
     * Set value.
     *
     * @param string|null $slotTime
     * @return $this
     */
    public function setSlotTime($slotTime);

    /**
     * Set value.
     *
     * @param string|null $slotDate
     * @return $this
     */
    public function setDate($slotDate);

    /**
     * Set value.
     *
     * @param int|null $slotId
     * @return $this
     */
    public function setSlotId($slotId);

    /**
     * Return value.
     *
     * @return string|null
     */
    public function getSlotTime();

    /**
     * Return value.
     *
     * @return string|null
     */
    public function getDate();

    /**
     * Return value.
     *
     * @return int|null
     */
    public function getSlotId();
}
