<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\TimeSlotDelivery\Api\Data;

/**
 * ResponseInterface interface.
 *
 * @api
 */
interface ResponseInterface
{
    /**
     * Get response.
     *
     * @return Webkul\TimeSlotDelivery\Api\Data\ResponseInterface
     */
    public function getResponse();
}
