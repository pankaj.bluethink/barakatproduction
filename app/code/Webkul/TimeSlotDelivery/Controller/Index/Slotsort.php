<?php

namespace Webkul\TimeSlotDelivery\Controller\Index;

use Magento\Framework\App\Action\Action;
use \Webkul\TimeSlotDelivery\Model\ResourceModel\AbstractCollection;
use Webkul\TimeSlotDelivery\Model\ResourceModel\TimeSlots\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

class Slotsort extends Action
{


    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Webkul\TimeSlotDelivery\Model\TimeSlots',
            'Webkul\TimeSlotDelivery\Model\ResourceModel\TimeSlots'
        );
    }

    public function execute()
    {
  $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
    $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 
    $data=$this->getRequest()->getPost();
     
     $weekday=array();
            $weekday['Mon']="Monday";
            $weekday['Tue']="Tuesday";
            $weekday['Wed']="Wednesday";
            $weekday['Thu']="Thursday";
            $weekday['Fri']="Friday";
            $weekday['Sat']="Saturday";
            $weekday['Sun']="Sunday";
           
     // if ($data['datepic']=="") {
     //     echo "Please select date";
     //     exit;
     // }
     //
     //        if ($data['resginid']=="") {
     //     echo "Please select resgin";
     //     exit;
     // }
     // if ($data['cityidget']=="") {
     //     echo "Please select city";
     //     exit;
     // }

     if ($data['resginid']=='609') {
         $location="Abu Dhabi";
     }
     if ($data['resginid']=='610') {
         $location="Dubai";
     }
      if ($data['resginid']=='611') {
         $location="Sharjah";
     }
     $date = $data['datepic'];
 $sday= date('D', strtotime($date));
//echo $location."==".$weekday[$sday];
     $result1 = $connection->fetchAll("SELECT *
FROM `webkul_timeslotdelivery_timeslots`
WHERE `delivery_day` = '".$weekday[$sday]."'and `location` = '".$location."' ;");
     
    $slot="";
    $slot.="<option value='topCaption'>Please Select Slot</option>";
     foreach ($result1 as $timeslotdata) {
        $slot.="<option value=".$timeslotdata['entity_id'].">".$timeslotdata['start_time']."-".$timeslotdata['end_time']."</option>";
        
     }
    echo $slot;

    }
}
