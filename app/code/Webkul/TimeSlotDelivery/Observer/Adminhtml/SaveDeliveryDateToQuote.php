<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_TimeSlotDelivery
 * @author Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\TimeSlotDelivery\Observer\Adminhtml;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;

class SaveDeliveryDateToQuote implements ObserverInterface
{
    /**
     * @var SessionManager
     */
    protected $_coreSession;

    /**
     * @param SessionManager $coreSession
     */
    public function __construct(
        SessionManager $coreSession
    ) {
        $this->_coreSession = $coreSession;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        $slotData = $observer->getRequestModel()->getParams();
        if (isset($slotData['date'])) {
            $obj = (object) $slotData;
            $quote = $observer->getOrderCreateModel()->getQuote();
            $quote->setOrderDeliveryDate($slotData['date']);
            $quote->setOrderDeliveryTime($slotData['slot_time']);
            if ($this->_coreSession->getSlotInfo()) {
                $this->_coreSession->unsSlotInfo();
                $this->_coreSession->setSlotInfo($obj);
            } else {
                $this->_coreSession->setSlotInfo($obj);
            }
        }
    }
}
