<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_TimeSlotDelivery
 * @author Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\TimeSlotDelivery\Block\Adminhtml;

use Webkul\TimeSlotDelivery\Model\ResourceModel\TimeSlots\CollectionFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Webkul\TimeSlotDelivery\Api\TimeSlotsManagementInterface;

class Admintimeslot extends \Magento\Sales\Block\Adminhtml\Order\Create\Shipping\Method\Form
{
    /**
     * @var \Magento\Quote\Model\Quote
     */
    public $quote;
    /**
     * @var \Webkul\TimeSlotDelivery\Helper\Data
     */
    public $helper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var CollectionFactory
     */
    protected $_timeSlotCollection;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_timezone;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \Magento\Framework\ObjectManagerInterface          $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime        $date
     * @param CollectionFactory                                  $timeSlotCollection
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Sales\Model\AdminOrder\Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Tax\Helper\Data $taxData,
        array $data = [],
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        CollectionFactory $timeSlotCollection,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        SessionManager $coreSession,
        \Webkul\TimeSlotDelivery\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    ) {
        $this->helper = $helper;
        $this->quote = $sessionQuote->getQuote();
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->_date = $date;
        $this->_timeSlotCollection = $timeSlotCollection;
        $this->_objectManager = $objectManager;
        $this->_coreSession = $coreSession;
        $this->_timezone = $timezone;
        parent::__construct(
            $context,
            $sessionQuote,
            $orderCreate,
            $priceCurrency,
            $taxData,
            $data
        );
    }

    public function getConfig()
    {
        if ($this->getRequest()->getParam('store_id')) {
            $store = $this->getRequest()->getParam('store_id');
        } else {
            $store = $this->getStoreId();
        }
        $allowedDays = $this->scopeConfig->getValue(TimeSlotsManagementInterface::XPATH_ALLOWED_DAY, ScopeInterface::SCOPE_STORE, $store);
        $processTime = $this->scopeConfig->getValue(TimeSlotsManagementInterface::XPATH_PROCESS_TIME, ScopeInterface::SCOPE_STORE, $store);
        $maxDays = $this->scopeConfig->getValue(TimeSlotsManagementInterface::XPATH_MAX_DAYS, ScopeInterface::SCOPE_STORE, $store);
        $message = $this->scopeConfig->getValue(TimeSlotsManagementInterface::XPATH_MESSAGE, ScopeInterface::SCOPE_STORE, $store);
        $isEnabled = (bool)$this->scopeConfig->getValue(TimeSlotsManagementInterface::ENABLED, ScopeInterface::SCOPE_STORE, $store);

        if (!$processTime) {
            $processTime=0;
        }

        $date = strtotime("+".$processTime." day", strtotime(date('Y-m-d')));

        $config = [
            'slotData' => [],
            'allowed_days' => explode(',', $allowedDays),
            'process_time' => $processTime,
            'start_date'   => date("Y-m-d", $date),
            'max_days'     => $maxDays,
            'slotEnabled'    => $isEnabled
        ];

        if (!$isEnabled) {
            return $config;
        }
        $allowedDays = explode(',', $allowedDays);

        $date = $this->_date;

        $collection = $this->_timeSlotCollection->create()
            ->addFieldToFilter('status', ['eq' => 1]);

        $createSlotData = [];
        $dateWiseSlots = [];

        $startDate = '';
        $startDate = date("Y-m-d", strtotime("+".$processTime." day", strtotime(date('Y-m-d'))));
        if ($collection->getSize()) {
            foreach ($collection as $slot) {
                if (!in_array($slot->getDeliveryDay(), $allowedDays)) {
                    continue;
                }
                $startTime = $date->gmtDate('g:i A', $slot->getStartTime());
                $endTime = $date->gmtDate('g:i A', $slot->getEndTime());
                $unique = 1;
                for ($i=0; $i <= $maxDays; $i++) {
                    $d = strtotime("+".$i." day", strtotime(date('Y-m-d')));
                    if (ucfirst($slot->getDeliveryDay()) == date('l', $d)) {
                        $isAvailable = $this->checkAvailabilty($slot, $d);
                        if (strtotime(date('Y-m-d', $d)) >= strtotime($startDate)) {
                            $dateWiseSlots[date('Y-m-d', $d)][] = [
                              'slot'=>$startTime.'-'.$endTime,
                              'is_available'=>$isAvailable,
                              'slot_id'   => $slot->getEntityId(),
                              'slot_group' => 'slot_'.$unique
                            ];
                            $unique++;
                        }
                    }
                }
            }
        }
        ksort($dateWiseSlots);
        $config['slotData']['slots'] = $dateWiseSlots;
        $config['slotData']['start_date'] = $startDate;
        $config['slotData']['message'] = $message;
        $config['slotcount'] = count($dateWiseSlots);

        return $config;
    }

    public function getStoreId()
    {
        return $this->storeManager->getStore()->getStoreId();
    }

    private function checkAvailabilty($slot, $date)
    {
        $date = $this->_date->gmtDate(date('Y-m-d', $date));
        $collection = $this->_objectManager->create(
            'Webkul\TimeSlotDelivery\Model\Order'
        )->getCollection()
        ->addFieldToFilter('slot_id', ['eq' => $slot->getEntityId()])
        ->addFieldToFilter('selected_date', ['eq' => $date]);
        if ($collection->getSize() >= $slot->getOrderCount()) {
            return 0;
        }

        $currentDate = $this->_timezone->date()->format('Y-m-d');
        $currentTime = $this->_timezone->date()->format('H:i:s');
        if (strtotime($date) == strtotime($currentDate)) {
            if (strtotime($currentTime) > strtotime($slot->getStartTime()) && strtotime($currentTime) < strtotime($slot->getEndTime())) {
                return 0;
            } elseif (strtotime($currentTime) > strtotime($slot->getEndTime())) {
                return 0;
            }
        }



        return 1;
    }

    public function getSlotInfo()
    {
        return $this->_coreSession->getSlotInfo();
    }
}
