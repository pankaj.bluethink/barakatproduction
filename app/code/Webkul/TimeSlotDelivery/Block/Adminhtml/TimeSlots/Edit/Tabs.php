<?php
 
namespace Webkul\TimeSlotDelivery\Block\Adminhtml\TimeSlots\Edit;
 
use Magento\Backend\Block\Widget\Tabs as WigetTabs;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Accordion;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Translate\InlineInterface;
 
class Tabs extends WigetTabs
{
    /**
     * @var string
     */
    protected $_template = 'Webkul_TimeSlotDelivery::timeslots/edit/tabs.phtml';

    public function __construct(
        Context $context,
        EncoderInterface $jsonEncoder,
        Session $authSession,
        InlineInterface $translateInline,
        array $data = []
    ) {
        $this->_translateInline = $translateInline;
        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('time_slot_config_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Delivery Time Slots'));
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareLayout()
    {
        $this->addTab(
            'time_slots',
            [
                'label' => __('Add/Remove Time Slots'),
                'title' => __('General'),
                'content' => $this->_translateHtml(
                    $this->getLayout()->createBlock(
                        \Webkul\TimeSlotDelivery\Block\Adminhtml\TimeSlots\Tab\Configuration::class
                    )->toHtml()
                ),
                'class' => 'user-defined',
                'active' => true,
                'group_code' => 'basic'
            ]
        );
        $this->addTab(
            'hello',
            [
                'label' => __('Time Slot Orders'),
                'url' => $this->getUrl('timeslots/*/grid', ['_current' => true]),
                'class' => 'ajax',
                'group_code' => 'basic'
            ]
        );
        return parent::_prepareLayout();
    }
 
    /**
     * @param string $parentTab
     * @return string
     */
    public function getAccordion($parentTab)
    {
        $html = '';
        foreach ($this->_tabs as $childTab) {
            if ($childTab->getParentTab() === $parentTab->getId()) {
                $html .= $this->getChildBlock('child-tab')->setTab($childTab)->toHtml();
            }
        }
        return $html;
    }

    /**
     * Translate html content
     *
     * @param string $html
     * @return string
     */
    protected function _translateHtml($html)
    {
        $this->_translateInline->processResponseBody($html);
        return $html;
    }
}
