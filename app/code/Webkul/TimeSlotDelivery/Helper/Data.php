<?php

namespace Webkul\TimeSlotDelivery\Helper;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Webkul\TimeSlotDelivery\Model\OrderFactory
     */
    protected $slotOrdersFactory;

    /**
     * @param \Magento\Framework\App\Helper\Context   $context
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Webkul\TimeSlotDelivery\Api\TimeSlotsRepositoryInterface $timeSlotRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\TimeSlotDelivery\Model\OrderFactory $slotOrdersFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_backendUrl = $backendUrl;
        $this->storeManager = $storeManager;
        $this->timeSlotRepository = $timeSlotRepository;
        $this->_date = $date;
        $this->slotOrdersFactory = $slotOrdersFactory;
    }
    /**
     * get products tab Url in admin
     * @return string
     */
    public function getTimeSlotFormUrl()
    {
        return $this->_backendUrl->getUrl('timeslots/timeslots/form', ['_current' => true]);
    }

    /**
     * Retrieve information from time slot configuration.
     *
     * @param string $field
     *
     * @return void|false|string
     */
    public function getConfigData($field)
    {
        $path = 'time_slots_delivery/general/'.$field;

        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * Check if selected slot is available or not
     * @throws NoSuchEntityException
     * @param int $slotId
     * @return bool
     *
     */
    public function isSlotAvailable($slotId, $date)
    {
        try {
            $slot = $this->timeSlotRepository->getById($slotId);
            if ($slot->getId() > 0) {
                $selectedDate = $this->_date->gmtDate(date('Y-m-d', strtotime($date)));
                $collection = $this->slotOrdersFactory->create()
                    ->getCollection()
                    ->addFieldToFilter('slot_id', ['eq' => $slotId])
                    ->addFieldToFilter('selected_date', ['eq' => $selectedDate]);
                
                if ($collection->getSize() >= $slot->getOrderCount()) {
                    return false;
                }
                return true;
            }
        } catch (\NoSuchEntityException $e) {
            throw new NoSuchEntityException(
                __('Selected Slot not available')
            );
        }
        
        return false;
    }
}
