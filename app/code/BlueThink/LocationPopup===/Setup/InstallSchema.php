<?php
/**
 * Copyright © 2015 BlueThink. All rights reserved.
 */

namespace BlueThink\LocationPopup\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
	
        $installer = $setup;

        $installer->startSetup();

		/**
         * Create table 'locationpopup_location'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('locationpopup_location')
        )
		->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'locationpopup_location'
        )
		->addColumn(
            'location_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'location_name'
        )
		->addColumn(
            'location_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'location_value'
        )
		/*{{CedAddTableColumn}}}*/
		
		
        ->setComment(
            'BlueThink LocationPopup locationpopup_location'
        );
		
		$installer->getConnection()->createTable($table);
		/*{{CedAddTable}}*/

        $installer->endSetup();

    }
}
