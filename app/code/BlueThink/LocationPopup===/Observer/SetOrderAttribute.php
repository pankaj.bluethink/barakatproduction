<?php
namespace BlueThink\LocationPopup\Observer;
 
class SetOrderAttribute implements \Magento\Framework\Event\ObserverInterface
{
    protected $_coreSession;

    /**
    * @var \Magento\Framework\Stdlib\CookieManagerInterface
    */
    protected $_cookieManager;

    public function __construct(
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
    )
    {
        $this->_coreSession = $coreSession;
        $this->_cookieManager = $cookieManager;
    }


    const COOKIE_NAME = "orderLocation";
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        $this->_coreSession->start();
        $data = $this->_coreSession->getStoreValue();

        if (!empty($data)) {
            $order->setLocation($data);
        }else{
            $cookieValue = $this->_cookieManager->getCookie(self::COOKIE_NAME);
            $order->setLocation($cookieValue);
        }
        return $this;
    }
}