<?php
/**
 * Copyright © 2015 BlueThink. All rights reserved.
 */
namespace BlueThink\LocationPopup\Model\ResourceModel;

/**
 * Location resource
 */
class Location extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('locationpopup_location', 'id');
    }

  
}
