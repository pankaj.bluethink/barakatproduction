require(
    [
    'jquery',
    'Magento_Ui/js/modal/modal',
    'loader',
    'domReady!'
    ],
    function(
        $,
        modal
        ) {
        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            modalClass: 'custom-popup-modal',
            buttons: []
        };
        var popup = modal(options, $('#custom-popup-modal'));
        if (getCookie("isShow").length <= 0) {
            $( document ).ready(function() {
                $('#custom-popup-modal').modal('openModal');
            }); 
        }
        $('#custom-popup-modal input[type="radio"]').click(function(event){
            event.preventDefault();
            setCookie("isShow", "Nope", 365);
            var store = $("input[name='store']:checked").val();
            setCookie("orderLocation", store, 365);
            if(store.length > 0){
                $.ajax({
                    type:"POST",
                    url: "<?php echo "http://13.233.165.42/locationpopup/index/change" ?>",
                    data: {store_id: store},
                    beforeSend:function(){
                        $("#custom-popup-modal").loader("show");
                        $('#custom-popup-modal').trigger('processStart');
                    },
                    success:function(response){
                        if (response.status && response.statusCode == 200) {
                            location.reload();
                        }
                    },
                    complete:function(){
                        $("#custom-popup-modal").loader("hide");
                    }
                });
            }
        });
        
    }
    );

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}