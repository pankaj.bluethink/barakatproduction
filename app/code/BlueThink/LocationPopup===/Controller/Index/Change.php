<?php
namespace BlueThink\LocationPopup\Controller\Index;


class Change extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;

    protected $_coreSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_coreSession = $coreSession;
        return parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPost('store_id');
        $resultJson = $this->resultJsonFactory->create();
        if (!empty($data)) {
                $this->_coreSession->start();
                $this->_coreSession->setStoreValue($data);
                $response = [    'status' => true,
                                'message' => "successfully store chnaged",
                                'url'   => NULL,
                                'statusCode' => 200
                            ];
                $resultJson->setData($response);
                return $resultJson;
        }else{
            $response = [   'status' => false,
                            'message' => "something went wrong",
                            'url'   => $this->_coreSession->getStoreValue(),
                            'statusCode' => 404
                        ];
            $resultJson->setData($response);
            return $resultJson;
        }
    }
}