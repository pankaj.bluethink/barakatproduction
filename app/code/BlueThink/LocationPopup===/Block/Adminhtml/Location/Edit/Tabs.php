<?php
namespace BlueThink\LocationPopup\Block\Adminhtml\Location\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_location_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Store Location'));
    }
}