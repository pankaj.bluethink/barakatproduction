<?php
namespace BlueThink\LocationPopup\Block\Adminhtml;
class Location extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
		
        $this->_controller = 'adminhtml_location';/*block grid.php directory*/
        $this->_blockGroup = 'BlueThink_LocationPopup';
        $this->_headerText = __('Store Location');
        $this->_addButtonLabel = __('Add Location'); 
        parent::_construct();
		
    }
}
