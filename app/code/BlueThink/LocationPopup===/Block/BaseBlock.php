<?php
/**
 * Copyright © 2015 BlueThink . All rights reserved.
 */
namespace BlueThink\LocationPopup\Block;
use Magento\Framework\UrlFactory;
class BaseBlock extends \Magento\Framework\View\Element\Template
{
	/**
     * @var \BlueThink\LocationPopup\Helper\Data
     */
	 protected $_devToolHelper;
	 
	 /**
     * @var \Magento\Framework\Url
     */
	 protected $_urlApp;
	 
	 /**
     * @var \BlueThink\LocationPopup\Model\Config
     */
    protected $_config;

     /**
     * @var \BlueThink\LocationPopup\Model\Location
     */
    protected $_location;


    /**
     * @param \BlueThink\LocationPopup\Block\Context $context
	 * @param \Magento\Framework\UrlFactory $urlFactory
     */
    public function __construct( 
    	\BlueThink\LocationPopup\Block\Context $context,
    	\BlueThink\LocationPopup\Model\LocationFactory $location

	)
    {
        $this->_devToolHelper = $context->getLocationPopupHelper();
		$this->_config = $context->getConfig();
        $this->_urlApp=$context->getUrlFactory()->create();
        $this->_location = $location;
		parent::__construct($context);
	
    }
	
	/**
	 * Function for getting event details
	 * @return array
	 */
    public function getEventDetails()
    {
		return  $this->_devToolHelper->getEventDetails();
    }
	
	/**
     * Function for getting current url
	 * @return string
     */
	public function getCurrentUrl(){
		return $this->_urlApp->getCurrentUrl();
	}
	
	/**
     * Function for getting controller url for given router path
	 * @param string $routePath
	 * @return string
     */
	public function getControllerUrl($routePath){
		
		return $this->_urlApp->getUrl($routePath);
	}
	
	/**
     * Function for getting current url
	 * @param string $path
	 * @return string
     */
	public function getConfigValue($path){
		return $this->_config->getCurrentStoreConfigValue($path);
	}
	
	/**
     * Function canShowLocationPopup
	 * @return bool
     */
	public function canShowLocationPopup(){
		$isEnabled=$this->getConfigValue('locationpopup/module/is_enabled');
		if($isEnabled)
		{
			$allowedIps=$this->getConfigValue('locationpopup/module/allowed_ip');
			 if(is_null($allowedIps)){
				return true;
			}
			else {
				$remoteIp=$_SERVER['REMOTE_ADDR'];
				if (strpos($allowedIps,$remoteIp) !== false) {
					return true;
				}
			}
		}
		return false;
	}

	public function getStore()
	{
		$location = $this->_location->create()->getCollection();
		$stores = [];
		foreach ($location as $group) {
			$store = [];
	        $store['name'] = $group->getLocationName();
	        $store['code'] = $group->getLocationValue();
	        $stores[] = $store;
    	}
    	return $stores;
	}
}
