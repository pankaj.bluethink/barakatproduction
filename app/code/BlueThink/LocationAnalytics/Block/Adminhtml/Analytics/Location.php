<?php
/**
 * Copyright © 2015 BlueThink . All rights reserved.
 */
namespace BlueThink\LocationAnalytics\Block\Adminhtml\Analytics;
class Location extends \Magento\Backend\Block\Template
{
	/**
	* @var \Magento\Customer\Model\Visitor
	*/
	protected $visitor;

	/**
	* @var \Magento\Customer\Model\Customer
	*/
	protected $customer;

	protected $_collectionFactory;

	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Customer\Model\Visitor $visitor,
		\Magento\Customer\Model\Customer $customer,
		\Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $collectionFactory,
        array $data = []
	){
		
		parent::__construct($context,$data);
		$this->visitor = $visitor;
		$this->customer = $customer;
		$this->_collectionFactory = $collectionFactory;
	}

	/**
	* getTodayVisitorCount
	* @param null
	* @return int 
	*/

	public function getTodayVisitorCount()
	{
		$visitors = $this->visitor->getCollection();
		$count = 0;
		foreach ($visitors as $visitor) {
			if (date('Y-m-d',strtotime($visitor->getLastVisitAt())) == date('Y-m-d')) {
				$count++;
			}
		}
		return $count;
	}

	/**
	* getCurrentMonthsVisitorCount
	* @param null
	* @return int 
	*/

	public function getCurrentMonthsVisitorCount()
	{
		$visitors = $this->visitor->getCollection();
		$count = 0;
		$lastMonthDate = date('Y-m-d',strtotime('-1 month',strtotime(date('Y-m-d'))));
		foreach ($visitors as $visitor) {
			if (date('Y-m-d',strtotime($visitor->getLastVisitAt())) <= date('Y-m-d') && date('Y-m-d',strtotime($visitor->getLastVisitAt())) >= $lastMonthDate) {
				$count++;
			}
		}
		return $count;
	}

	/**
	* getVisitorCount
	* @param null
	* @return int 
	*/

	public function getVisitorCount()
	{
		$visitors = $this->visitor->getCollection();
		return count($visitors->getData());
	}	


	/**
	* getAllCustomerCount
	* @param null
	* @return int 
	*/

	public function getAllCustomerCount()
	{
		return $this->customer->getCollection()->getSize();
	}


	/**
	* getCurrentMonthCustomerCount
	* @param null
	* @return int 
	*/

	public function getCurrentMonthCustomerCount()
	{
		$customerCollection = $this->customer->getCollection();
		$count = 0;
		$lastMonthDate = date('Y-m-d',strtotime('-1 month',strtotime(date('Y-m-d'))));
		foreach ($customerCollection as $customer) {
			if (date('Y-m-d',strtotime($customer->getCreatedAt())) <= date('Y-m-d') && date('Y-m-d',strtotime($customer->getCreatedAt())) >= $lastMonthDate) {
				$count++;
			}
		}
		return $count;
	}

	/**
	* getTodayCustomerCount
	* @param null
	* @return int 
	*/

	public function getTodayCustomerCount()
	{
		$customerCollection = $this->customer->getCollection();
		$count = 0;
		foreach ($customerCollection as $customer) {
			if (date('Y-m-d',strtotime($customer->getCreatedAt())) == date('Y-m-d')) {
				$count++;
			}
		}
		return $count;
	}	

	/**
	* getBestSellerProduct
	* @param null
	* @return collection 
	*/

	public function getBestSellerProduct()
	{
		$bestSellerProdcutCollection = $this->_collectionFactory->create()
                    ->setModel('Magento\Catalog\Model\Product')
                    ->setPeriod('month')->load();
       return $bestSellerProdcutCollection;
	}		
}


