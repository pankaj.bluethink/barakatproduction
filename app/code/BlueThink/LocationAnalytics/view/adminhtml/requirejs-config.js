var config = {
    map: {
        '*': {
	    		chart: 	'BlueThink_LocationAnalytics/js/chart',
	    		graph: 	'BlueThink_LocationAnalytics/js/graph',
        		logger: 'BlueThink_LocationAnalytics/js/logger',
        }
    }
};

