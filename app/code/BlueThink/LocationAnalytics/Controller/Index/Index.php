<?php
namespace BlueThink\LocationAnalytics\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
    public function __construct(
        \Magento\Framework\App\Action\Context $context,      
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Data\Form\FormKey $formkey,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\Service\OrderService $orderService , 
         \Magento\Framework\ObjectManagerInterface $ObjectManagerInterface
    )
    {
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_formkey = $formkey;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->orderService = $orderService;
        $this->objectmanager = $ObjectManagerInterface;
        return parent::__construct($context);

    }

    public function execute()
    {
         
         // $order = $this->objectmanager->create('BlueThink\LocationAnalytics\Model\Orderdownload');
         // $order->downloadOrder();

    }

     
}
 
