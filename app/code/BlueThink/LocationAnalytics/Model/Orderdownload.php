<?php
/**
 * @category   Webstraxt
 * @package    Webstraxt_BASalesPresenterSync
 * @author     baliram@webstraxt.com
 * @copyright  Webstraxt Limited https://webstraxt.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * Copyright © 2019 Webstraxt. All rights reserved.
 */

namespace BlueThink\LocationAnalytics\Model;
use Magento\Framework\App\Filesystem\DirectoryList;

class Orderdownload
{
    /**
    * @var \Webstraxt\BASalesPresenterSync\Model\LogModel
    */
    protected $_logger;

    /**
    * @var \Magento\Framework\Filesystem
    */
    protected $_filesystem;

    /**
    * @var \Webstraxt\BASalesPresenterSync\Model\ExportCurl
    */
    protected $_exportCurl;

    /**
    * @param Magento\Framework\App\Helper\Context $context
    * @param Magento\Store\Model\StoreManagerInterface $storeManager
    * @param Magento\Catalog\Model\Product $product
    * @param Magento\Framework\Data\Form\FormKey $formKey $formkey,
    * @param Magento\Quote\Model\Quote $quote,
    * @param Magento\Customer\Model\CustomerFactory $customerFactory,
    * @param Magento\Sales\Model\Service\OrderService $orderService,
    * @param Magento\Quote\Model\Cart\Currency $currency,
    * @param Magento\Customer\Model\AddressFactory $addressFactory,
    * @param Magento\Catalog\Model\ProductFactory $productFactory,
    * @param Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
    * @param Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
    * @param Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    */

    public function __construct(
      \Magento\Framework\View\Element\Template\Context $context,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
       \Magento\Framework\Filesystem $filesystem,      
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Catalog\Model\Product $product,
      \Magento\Catalog\Model\ProductFactory $productFactory,
      \Magento\Framework\Data\Form\FormKey $formkey,
      \Magento\Quote\Model\QuoteFactory $quote,
      \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
      \Magento\Quote\Model\QuoteManagement $quoteManagement,
      \Magento\Customer\Model\CustomerFactory $customerFactory,
      \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
      \Magento\Sales\Model\Service\OrderService $orderService,
      \Magento\Quote\Model\Cart\Currency $currency,
      \Magento\Customer\Model\AddressFactory $addressFactory,
      \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemFactory,
      \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
      \Psr\Log\LoggerInterface $logger,   
  
    \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender

    ) {
        $this->_scopeConfig = $scopeConfig;        
        $this->_filesystem = $filesystem;        
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_formkey = $formkey;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->orderService = $orderService;
        $this->_currency = $currency;
        $this->productRepository = $productRepository;
        $this->_addressFactory = $addressFactory;
        $this->_productFactory = $productFactory;
        $this->_cartItemFactory = $cartItemFactory;       
        $this->_stockRegistry = $stockRegistry;
        $this->logger = $logger;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->orderSender = $orderSender;
    }

    const ORDER_DOWNLOAD_LOG_FILE = "OrderDownloadLog.log";
   

    /**
    * downloadOrder
    * @param null
    * @return null
    */
       
    public function downloadOrder()
    {   

        $log = "";
        $row=0;      
        $handle = fopen("/var/www/html/pub/media/bluethink/master_order.csv","r");        
        $row = 0;
        while (($data = fgetcsv($handle)) !== FALSE)
        {
            
            if($row>1060)
             {
                /*echo"<pre>***************";
                print_r($data);
                exit;*/
                $this->createOrder($data);
                sleep(2);
               // $this->createOrder2($data);
             }
              
        $row ++;
        }
      
       
    }

    /**
    * createOrder
    * @param $order object
    * @return $status boolean
    */

    public function createOrder($order='')
    {
        $log = "";
        //$jsonEncode =json_encode($order);
        //$orderparse = json_decode($jsonEncode);
       // $storeId = $this->_scopeConfig->getStore()->getId();
         $storeId = $this->_storeManager->getStore()->getId();
        $store = $this->_storeManager->getStore($storeId);
        $websiteId = $this->_storeManager->getStore($store)->getWebsiteId();
        $customer=$this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail(trim($order[7]));

        if(!$customer->getEntityId()){
            //If not avilable then create this customer
            try {
               // $fullNameExlode = explode(" ", $order[5]);
                //$firstname = $lastname = "-";
                /*if (count($fullNameExlode) > 1) {
                    $firstname = $fullNameExlode[0];
                    $lastname = $fullNameExlode[1];
                }*/
              /*  if (strpos($order[5]," ") == false) {
                        $firstname = $order[5];
                        $lastname = $order[5];
                    }*/
                     $firstname = $order[5];
                     $lastname = $order[5];

                $customer->setWebsiteId($websiteId)
                        ->setStore($store)
                        ->setFirstname($firstname)
                        ->setLastname($lastname)
                        ->setEmail(trim($order[7])) 
                        ->setPassword("barakat@321"); 
                $customer->save();

                $address = $this->_addressFactory->create();

                $address->setCustomerId($customer->getId())
                        //->setFirstname($firstname)
                       // ->setLastname($lastname)
                        ->setCountryId('AE')
                        ->setPostcode('97991')
                        ->setCity($order[16])
                        ->setTelephone($order[6])
                        ->setRegion('AE')
                        //->setCompany($orderparse->CompanyName)
                       ->setStreet(array(
                                '0' => addslashes(substr($order[15],0,15)),
                                '1' => addslashes(substr($order[15],0,15))
                        ))
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1');
                       // ->setSaveInAddressBook('1');
                $address->save();

            } catch (\Exception $e) {
                $this->logger->info("Magento wasn't able to create customer, Exception raised ".$e->getMessage());
                return;
            }
        }

        $quote=$this->quote->create(); 
        $quote->setStore($store); //set store for which you create quote

        // if you have allready buyer id then you can load customer directly 
      /*  $fullNameExlode = explode(" ", $order[5]);
                $firstname = $lastname = "-";
                if (count($fullNameExlode) > 1) {
                    $firstname = $fullNameExlode[0];
                    $lastname = $fullNameExlode[1];
                }
                 if (strpos($order[5]," ") == false) {
                        $firstname = $order[5];
                        $lastname = $order[5];
                    }*/
                     $firstname = $order[5];
                     $lastname = $order[5];
               
        $customer =$this->customerFactory->create()->setWebsiteId($websiteId)->loadByEmail(trim($order[7]))->getDataModel();
        $address = $this->_addressFactory->create();
        $address->setCustomerId($customer->getId())
                       ->setFirstname($firstname)
                        ->setLastname($lastname)
                        ->setCountryId('AE')
                        ->setPostcode('97991')
                        ->setCity($order[16])
                        ->setTelephone($order[6])
                         ->setRegion('AE')
                       ->setStreet(array(
                                '0' => addslashes(substr($order[15],0,15)),
                                '1' => addslashes(substr($order[15],0,15))
                        ))
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');
                $address->save();

        $quote->setCurrency($this->_currency->setQuoteCurrencyCode('AED'));

        $quote->assignCustomer($customer); //Assign quote to customer

        $mainordernotes = $order[18];
      //  $itemcount = $order->SalesOrderLines->SalesOrderLine->count();
        $comment = "";
        $inventoryUpdate = array();

        //add items in quote
        if (!empty($order)) {
            $itemhandle = fopen("/var/www/html/pub/media/bluethink/orderitemcsv/item".$order[0].".csv","r");        
            $itemrow = 0;            
            while (($itemdata = fgetcsv($itemhandle)) !== FALSE)
            {  
                /*echo"<pre>***************";
                print_r($itemdata);
                exit;*/
                try {
                    $product = $this->_productFactory->create()->loadByAttribute('item_id',$itemdata[2]);
                    if(empty($product))
                    {
                    	continue;

                    }
                 //    echo"<pre>***************";
	                // print_r($product->getId());
	                // exit;
                } catch (Exception $e) {
                    $this->_logger->loginfo("No Product found with sku ". $itemdata[2]."Exception raised ".$e->getMessage(),"orderimport.log");
                    return;
                }

               /* if (gettype($value->SpecialInstructions) !== "object") {
                    $comment .= $value->ProductCode." - ". $value->SpecialInstructions."|";   
                }
                */
                $_product = $this->_product->load($product->getId());
                $_product->setStoreId($storeId)
                        ->setQuoteParentItemId(NULL)
                        ->setProductId($product->getId())
                        ->setProductType($product->getTypeId())
                        ->setQtyOrdered($itemdata[4])
                        ->setName($product->getName())
                        ->setSku($product->getSku())
                        ->setPrice($itemdata[3])
                        ->setBasePrice($product->getPrice())                       
                      //  ->setTaxPercent($value->SalesTaxRate)
                        ->setOriginalPrice($product->getPrice())
                        //->setTaxAmount($value->SalesTaxLineValue)
                        //->setBaseTaxAmount($value->SalesTaxLineValue)
                        ->setRowTotal($itemdata[3]*$itemdata[4])
                        ->setBaseRowTotal($itemdata[3]*$itemdata[4]);
               $quoteItem = $this->_cartItemFactory->create();
               $quoteItem->setProduct($product);
               $quoteItem->setqty($itemdata[4]);
               $quoteItem->setItemOrganisation($itemdata[7]);
               $quote->addItem($quoteItem);
               $row = [];
               $row['sku'] = $_product->getSku();
               $row['qty'] = $itemdata[4];
               $inventoryUpdate[] = $row;
            }
        }
        $billingAddressId = $customer->getDefaultBilling();
        $shippingAddressId = $customer->getDefaultShipping();
        $customerbillingAddress = $this->_addressFactory->create()->load($billingAddressId);
        $customershippingAddress = $this->_addressFactory->create()->load($shippingAddressId);
        //Set Address to quote
        $quote->getBillingAddress()->addData($customerbillingAddress->toArray());
        $quote->getShippingAddress()->addData($customershippingAddress->toArray());
        // Collect Rates and Set Shipping & Payment Method
        $shippingAddress=$quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
                        ->collectShippingRates()
                        ->setShippingMethod('freeshipping_freeshipping'); //shipping method        
        $quote->setPaymentMethod('cashondelivery'); //payment method
        $quote->setInventoryProcessed(false); //not effetc inventory
        $quote->save(); //Now Save quote and your quote is ready 
        // Set Sales Order Payment
        $quote->getPayment()->importData(['method' => 'cashondelivery']); 
        // Collect Totals & Save Quote
        $quote->collectTotals()->save(); 
       // $orderCloud = $this->_ordercloudstatus->create()->load($orderparse->ID,'order_id');
       // if (empty($orderCloud->getData())) {
            // Create Order From Quote
            $order = $this->quoteManagement->submit($quote); 
           // $order->setDeliverySlot($order[13]);          
           // $order->setEmailSent(0);
           // $order->addStatusHistoryComment($mainordernotes);
            if($order->getEntityId()){
                return $order->getRealOrderId();
            }
      //  }      
    }

    /**
    * updateInventory
    * @param InventoryData
    * @return null
    */
    public function updateInventory($InventoryData='')
    {
        if (is_array($InventoryData)) {
            foreach ($InventoryData as $value) {
                $stockItem = $this->_stockRegistry->getStockItemBySku($value['sku']);
                $StockQty = $stockItem->getQty();
                $finalQty = $StockQty - $value['qty'];
                $stockItem->setQty($finalQty);
                $stockItem->setIsInStock((bool)$value['qty']); // this line
                $this->_stockRegistry->updateStockItemBySku($value['sku'], $stockItem);
            }
        }
    }

     public  function clean($string) {
     //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
   }

   public function createOrder2($order) {
              $fullNameExlode = explode(" ", $order[5]);
                $firstname = $lastname = "-";
                if (count($fullNameExlode) > 1) {
                    $firstname = $fullNameExlode[0];
                    $lastname = $fullNameExlode[1];
                }
                 if (strpos($order[5]," ") == false) {
                        $firstname = $order[5];
                        $lastname = $order[5];
                    }
        $store = $this->_storeManager->getStore();
        $storeId = $store->getStoreId();
        $websiteId =  $this->_storeManager->getStore()->getWebsiteId();
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail(trim($order[7]));// load customet by email address
        if(!$customer->getId()){
            //For guest customer create new cusotmer
            $customer->setWebsiteId($websiteId)
                    ->setStore($store)
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setEmail(trim($order[7]))
                    ->setPassword("admin@786");
            $customer->save();
        }
        $quote=$this->quote->create(); //Create object of quote
        $quote->setStore($store); //set store for our quote
        /* for registered customer */
        $customer= $this->customerRepository->getById($customer->getId());
        $quote->setCurrency();
        $quote->assignCustomer($customer); //Assign quote to customer

        if (!empty($order)) {
            $itemhandle = fopen("/var/www/html/pub/media/bluethink/orderitemcsv/item".$order[0].".csv","r");        
            $itemrow = 0;            
            while (($itemdata = fgetcsv($itemhandle)) !== FALSE)
            {             
                 try {
                    $products = $this->_productFactory->create()->loadByAttribute('item_id',$itemdata[2]);
                    if(empty($product))
                    {
                        continue;

                    }
                 //    echo"<pre>***************";
                    // print_r($product->getId());
                    // exit;
                } catch (Exception $e) {
                    $this->_logger->loginfo("No Product found with sku ". $itemdata[2]."Exception raised ".$e->getMessage(),"orderimport.log");
                    return;
                }
                  $product=$this->productRepository->getById($products->getId());
                $quote->addProduct($product,intval($itemdata[4]));
            }
        }

        //Set Billing and shipping Address to quote
        $quote->getBillingAddress()->addData([$order[15]]);
        $quote->getShippingAddress()->addData([$order[15]]);

        // set shipping method
        $shippingAddress=$quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
                        ->collectShippingRates()
                        ->setShippingMethod('flatrate_flatrate'); //shipping method, please verify flat rate shipping must be enable
        $quote->setPaymentMethod('cashondelivery'); //payment method, please verify checkmo must be enable from admin
        $quote->setInventoryProcessed(false); //decrease item stock equal to qty
        $quote->save(); //quote save 
        // Set Sales Order Payment, We have taken check/money order
        $quote->getPayment()->importData(['method' => 'cashondelivery']);
 
        // Collect Quote Totals & Save
        $quote->collectTotals()->save();
        // Create Order From Quote Object
        $order = $this->quoteManagement->submit($quote);
        /* for send order email to customer email id */
      //  $this->orderSender->send($order);
        /* get order real id from order */
        //$orderId = $order->getIncrementId();
       /* if($orderId){
            $result['success']= $orderId;
        }else{
            $result=['error'=>true,'msg'=>'Error occurs for Order placed'];
        }
        return $result;*/
    }
}

