<?php

namespace BlueThink\CustomerAttribute\Setup;



use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    
    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }
 
    
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        
        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'existing_system_customer_id', [
            'type' => 'varchar', // type of attribute
            'label' => 'Existing System Customer Id',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
             
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'existing_system_customer_id')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();

        
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'emirate', [
            'type' => 'varchar', // type of attribute
            'label' => 'Emirate',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'emirate')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();



        
         $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'google_map_link', [
            'type' => 'varchar', // type of attribute
            'label' => 'Google Map Link',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'google_map_link')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();



               
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'activated', [
            'type' => 'varchar', // type of attribute
            'label' => 'Activated',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'activated')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();



                        
    $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'item_organisation_id', [
            'type' => 'varchar', // type of attribute
            'label' => 'Item Organisation Id',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'item_organisation_id')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();




                       
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'item_origin_id', [
            'type' => 'varchar', // type of attribute
            'label' => 'Item Origin Id',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'item_origin_id')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();
                   
        
         $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'billing_location', [
            'type' => 'varchar', // type of attribute
            'label' => 'Billing Location',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'billing_location')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();



                            
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'billing_emirate', [
            'type' => 'varchar', // type of attribute
            'label' => 'Billing Emirate',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'billing_emirate')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();



                            
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'shipping_location', [
            'type' => 'varchar', // type of attribute
            'label' => 'Shipping Location',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'shipping_location')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();


                            
        
         $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'shipping_emirate', [
            'type' => 'varchar', // type of attribute
            'label' => 'Shipping Emirate',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'shipping_emirate')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();




                            
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'delivery_area', [
            'type' => 'varchar', // type of attribute
            'label' => 'Delivery Area',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'delivery_area')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();




            
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'is_barakat_staff', [
            'type' => 'varchar', // type of attribute
            'label' => 'Is Barakat Staff',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'is_barakat_staff')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();



                    
        
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'loyalty_point', [
            'type' => 'varchar', // type of attribute
            'label' => 'Loyalty Point',
            'input' => 'text', // input type
            'source' => '',
            'required' => false, // if you want to required need to set true
            'visible' => true,
            'position' => 500, // position of attribute
            'system' => false,
            'backend' => '',
             'is_used_in_grid' => true,
        ]);
        
        /* Specify which place you want to display customer attribute */
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'loyalty_point')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout'
            ]
        ]);
        $attribute->save();


        
        

    }
}








