<?php

namespace BlueThink\Ordersegrigation\Controller\Adminhtml\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\OrderManagementInterface;
use WeProvide\Dompdf\Controller\Result\DompdfFactory;
use WeProvide\Dompdf\Controller\Result\Dompdf;
use Magento\Framework\Controller\Result\JsonFactory;

class Orderfilter extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{

    protected $orderManagement;
    protected $dompdfFactory;
    protected $layoutFactory;
    protected $_resultJsonFactory;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OrderManagementInterface $orderManagement,       
        DompdfFactory $dompdfFactory,
        JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->orderManagement = $orderManagement;
        $this->dompdfFactory = $dompdfFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
    }
    
     public function getHtmlForPdf($collection) {
           
           $orderids= $this->getRequest()->getParam('selected'); 
           
           $pdfhtml = "";
        foreach ($orderids as $id) {
            $model = $this->_objectManager->create('Magento\Sales\Model\Order');
            if (!$id) {
                continue;
            }

            //$ordersheet ="";    
            //$ordersheetcustomer = ""; 
           
            $loadedOrder = $model->load($id);           
           /* echo "<pre>*************";
            print_r($loadedOrder->getData());
            exit;*/
           
            $orderdeliverydate = $loadedOrder->getData('order_delivery_date');
            $orderdeliveryschedule = $loadedOrder->getData('delivery_slot');
            $location = $loadedOrder->getData('location');
            $incrementId = $loadedOrder->getData('increment_id');
            $orderDate= $loadedOrder->getData('created_at');            
            $shipping = $loadedOrder->getShippingAddress();         
            $customerName = $loadedOrder->getCustomerFirstname();
            $customerlastName = $loadedOrder->getCustomerLastname();
            $customeremail = $loadedOrder->getCustomerEmail();

            if($loadedOrder->getShippingAddress())
            {
                $streetaddress1 = $loadedOrder->getShippingAddress()->getStreet(1);
                $streetaddress2 = $loadedOrder->getShippingAddress()->getStreet(2);            
                $country = $shipping->getCountryId();
            }

            $items = $loadedOrder->getAllItems();
            $itemnumbersequence ="";
            $itemnumber = 1;
            $itemdescription = "";
            $qty = "";
            foreach ($items as $item) {
                $name =  $item->getName();
                $qtyordered = $item->getQtyOrdered(); 
                $productadditonal = $item->getData('product_options');
                $itemnumbersequence .= "<h6>".$itemnumber."</h6>";
                $itemdescription .= "<h5>".$name."</h5>";
                $qty .="<h6>".$qtyordered."</h6>";
                $itemnumber++;              
            }

            $ordersheetcustomer = '<div class="order_sheet_head">
            <!--div class="c_logo">
                <img src="http://15.206.167.134/pub/media/logo/stores/3/image002.png"/>
            </div-->
            <div class="order_summary">
                <div class="order_summary_top">
                <div class="order_summary_top_left">
                    <h2 class="hed-1">Order Id</h2>
                    <h2 class="hed-2">#'.$incrementId.'</h2>
                    </div>
                    <div class="order_summary_top_right">
                    <h1>ABDM1</h1>
                    </div>
                </div>
                <div class="order_summary_bottom">
                    <div class="order_summary_bottom_left">
                    <p>Order Details</p>
                    <div class="order_summary_bottom_left_content">
                        <div class="sec_1">
                            <ul class="sec_1_list">
                                <li>Order Date</li>
                                <li>Delivery Date</li>
                                <li>Delivery Schedule</li>
                                <li>Zone</li>
                                <li>Emirate</li>
                            </ul>
                        </div>
                        <div class="sec_2">
                            <ul class="sec_2_list">
                                <li>'.$orderDate.'</li>
                                <li>'.$orderdeliverydate.'</li>
                                <li>'.$orderdeliveryschedule.'</li>
                                <li>'.$location.'</li>
                                <li>'.$location.'</li>
                            </ul>
                        </div>
                    </div>
                    </div>
                    <div class="order_summary_bottom_right">
                    <p>Customer Details</p>
                    <div class="c_detail_content">
                    <div class="c_detail_content_left">
                    <h4>'.$customerName.'</h4>                
                    </div>
                    <div class="c_detail_content_right">
                    <h3>'.$customerlastName.'</h3>
                    </div>
                    </div>
                    </div>
                    </div>
                    
                </div>
            </div>';

             $ordersheet='<div class="order_sheet_body">
            <div class="order_sheet_body_sec_1">
                <p>SNo.</p>
                <div class="order_sheet_body_sec_1_content">
                '.$itemnumbersequence .'
                </div>
            </div>
            <div class="order_sheet_body_sec_2">
                <p>Item Description</p>
                <div class="order_sheet_body_sec_2_content">
                    <h3>FRUITS & VEGETABLES</h3>
                    '.$itemdescription.'
                </div>
            </div>
            <div class="order_sheet_body_sec_3">
                <p>Quantity</p>
                <div class="order_sheet_body_sec_3_content">
                   '.$qty.'
                </div>
            </div>
            </div>';

            $html2='
            <div class="order_sheet">
                '.$ordersheetcustomer.'
                '.$ordersheet.'
                </div>';

            $pdfhtml.= $html2;   
           
        }

        $html = '<!DOCTYPE html>
            <html>
            <head>
             <title>jsPDF</title>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <style>
                @CHARSET "UTF-8";
                .page-break {
                    page-break-after: always;
                    page-break-inside: avoid;
                    clear:both;
                }
                .page-break-before {
                    page-break-before: always;
                    page-break-inside: avoid;
                    clear:both;
                }
            </style>
            <link rel="stylesheet" href="http://15.206.167.134/pub/media/bluethink/pdfdesign/pdf.css" type="text/css" media="all"/>
               </head>
            <body><div id ="html-2-pdfwrapper" style="display:none;position: absolute; left: 20px; top: 50px; bottom: 0; overflow: auto; width: 600px">      
            '.$pdfhtml.'</div>'.$this->generatepdf().'</body>
            </html>';       

           file_put_contents("/var/www/html/pub/media/bluethink/pdfdesign/pd.html", $html);
           sleep(2);
          // file_get_contents("/var/www/html/pub/media/bluethink/pdfdesign/pd.html");
         //$render = $this->_resultJsonFactory->create();
        // $render->setData(['output' => $html]);
           print_r(file_get_contents("/var/www/html/pub/media/bluethink/pdfdesign/pd.html"));
        // print_r($html);
         exit;
         return $render;
        //print_r($html);

         //exit;



        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        /*echo $storeManager->getStore()->getBaseUrl();
        exit;*/      

        //return $html;
            $response = $this->dompdfFactory->create();
            //$this->dompdfFactory->set_base_path("/var/www/html/pub/media/bluethink/pdfdesign/pdf.css");
            $response->setFileName($incrementId);
            $response->setData($html);
            return $response;
            //exit;
    }

   
    protected function massAction(AbstractCollection $collection)
    {
         
          return $this->getHtmlForPdf($collection);
         // exit;
            /*$response = $this->dompdfFactory->create();
            $response->setFileName('3000000017');
            $response->setData($this->getHtmlForPdf($collection));
            return $response;
            exit;*/
            /* $response = $this->dompdfFactory->create();
             $response->setFileName()
             $response->setData($this->getHtmlForPdf($collection));
            return $response;
            exit;*/
      /*  $countDeleteOrder = 0;
        $model = $this->_objectManager->create('Magento\Sales\Model\Order');
        foreach ($collection->getItems() as $order) {
            if (!$order->getEntityId()) {
                continue;
            }

             echo"************2<pre>";
             print_r($order->getEntityId());
             exit;
            $loadedOrder = $model->load($order->getEntityId());
            $loadedOrder->delete();
            $countDeleteOrder++;
        }
        $countNonDeleteOrder = $collection->count() - $countDeleteOrder;

        if ($countNonDeleteOrder && $countDeleteOrder) {
            $this->messageManager->addError(__('%1 order(s) cannot be done it .', $countNonDeleteOrder));
        } elseif ($countNonDeleteOrder) {
            $this->messageManager->addError(__('You done it  the order(s).'));
        }

        if ($countDeleteOrder) {
            $this->messageManager->addSuccess(__('We done it %1 order(s).', $countDeleteOrder));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->getComponentRefererUrl());
        return $resultRedirect;*/
    }

    public function generatepdf()
    {

        $jspdf='<script src="http://15.206.167.134/pub/media/bluethink/pdfdesign/dist/jspdf.min.js"></script>
                    <script>
                    margins = {
                      top: 120,
                      bottom: 40,
                      left: 30,
                      width: 1024
                    };
                    var base64Img = null;
                    imgToBase64("/var/www/html/pub/media/logo/stores/3/image002.png", function(base64) {
                        base64Img = base64; 
                    });
            generate = function()
            {
                var pdf = new jsPDF("p", "pt", "a4");
                pdf.setFontSize(18);
                pdf.fromHTML(document.getElementById("html-2-pdfwrapper"), 
                    margins.left, 
                    margins.top,
                    {
                        
                        width: margins.width
                    },function(dispose) {
                        headerFooterFormatting(pdf, pdf.internal.getNumberOfPages());
                    }, 
                    margins);
                    
                var iframe = document.createElement("iframe");
                iframe.setAttribute("style","position:absolute;right:0; top:0; bottom:0; height:100%; width:650px; padding:20px;");
                document.body.appendChild(iframe);
                
                iframe.src = pdf.output("datauristring");
            };
            function headerFooterFormatting(doc, totalPages)
            {
                for(var i = totalPages; i >= 1; i--)
                {
                    doc.setPage(i);               
                 
                    header(doc);
                    
                    footer(doc, i, totalPages);
                    doc.page++;
                }
            };

            function header(doc)
            {
                doc.setFontSize(30);
                doc.setTextColor(40);
                doc.setFontStyle("normal"); 
                if (base64Img) {
                   doc.addImage(base64Img, "png", margins.left, 10, 40,40);        
                }    
                doc.text("Order  sheet", margins.left + 50, 40 );
                doc.setLineCap(2);
                doc.line(3, 70, margins.width + 43,70);
            };

            function footer(doc, pageNumber, totalPages){

                var str = "Page " + pageNumber + " of " + totalPages
               
                doc.setFontSize(10);
                doc.text(str, margins.left, doc.internal.pageSize.height - 20);    
            };

            function imgToBase64(url, callback, imgVariable) {
 
    if (!window.FileReader) {
        callback(null);
        return;
    }
    var xhr = new XMLHttpRequest();
    xhr.responseType = "blob";
    xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
            imgVariable = reader.result.replace("text/xml", "image/png");
            callback(imgVariable);
        };
        reader.readAsDataURL(xhr.response);
    };
    xhr.open("GET", url);
    xhr.send();
};
            generate();
             </script>';
             return $jspdf;
    }
}