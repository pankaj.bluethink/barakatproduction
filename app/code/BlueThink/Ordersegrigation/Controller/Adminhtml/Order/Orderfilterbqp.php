<?php

namespace BlueThink\Ordersegrigation\Controller\Adminhtml\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\OrderManagementInterface;
use WeProvide\Dompdf\Controller\Result\DompdfFactory;
use WeProvide\Dompdf\Controller\Result\Dompdf;

class Orderfilterbqp extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{

    protected $orderManagement;
     protected $dompdfFactory;
    protected $layoutFactory;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OrderManagementInterface $orderManagement,       
        DompdfFactory $dompdfFactory
    ) {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->orderManagement = $orderManagement;
         $this->dompdfFactory = $dompdfFactory;
    }
    
     public function getHtmlForPdf($collection) {
           
           $orderids= $this->getRequest()->getParam('selected'); 
           
           $pdfhtml = "";
        foreach ($orderids as $id) {
            $model = $this->_objectManager->create('Magento\Sales\Model\Order');
            if (!$id) {
                continue;
            }

            //$ordersheet ="";    
            //$ordersheetcustomer = ""; 
           
            $loadedOrder = $model->load($id);           
           /* echo "<pre>*************";
            print_r($loadedOrder->getData());
            exit;*/
           
            $orderdeliverydate = $loadedOrder->getData('order_delivery_date');
            $orderdeliveryschedule = $loadedOrder->getData('delivery_slot');
            $location = $loadedOrder->getData('location');
            $incrementId = $loadedOrder->getData('increment_id');
            $orderDate= $loadedOrder->getData('created_at');            
            $shipping = $loadedOrder->getShippingAddress();         
            $customerName = $loadedOrder->getCustomerFirstname();
            $customerlastName = $loadedOrder->getCustomerLastname();
            $customeremail = $loadedOrder->getCustomerEmail();

            if($loadedOrder->getShippingAddress())
            {
                $streetaddress1 = $loadedOrder->getShippingAddress()->getStreet(1);
                $streetaddress2 = $loadedOrder->getShippingAddress()->getStreet(2);            
                $country = $shipping->getCountryId();
            }

            $items = $loadedOrder->getAllItems();
            $itemnumbersequence ="";
            $itemnumber = 1;
            $itemdescription = "";
            $qty = "";
            foreach ($items as $item) {
                $name =  $item->getName();
                $qtyordered = $item->getQtyOrdered(); 
                $productadditonal = $item->getData('product_options');
                $itemnumbersequence .= "<h6>".$itemnumber."</h6>";
                $itemdescription .= "<h5>".$name."</h5>";
                $qty .="<h6>".$qtyordered."</h6>";
                $itemnumber++;              
            }

            $ordersheetcustomer = '<div class="order_sheet_head">
            <div class="c_logo">
                <img src="http://15.206.167.134/pub/media/logo/stores/3/image002.png"/>
            </div>
            <div class="order_summary">
                <div class="order_summary_top">
                <div class="order_summary_top_left">
                    <h2 class="hed-1">Order Sheet</h2>
                    <h2 class="hed-2">#'.$incrementId.'</h2>
                    </div>
                    <div class="order_summary_top_right">
                    <h1>ABDM1</h1>
                    </div>
                </div>
                <div class="order_summary_bottom">
                    <div class="order_summary_bottom_left">
                    <p>Order Details</p>
                    <div class="order_summary_bottom_left_content">
                        <div class="sec_1">
                            <ul class="sec_1_list">
                                <li>Order Date</li>
                                <li>Delivery Date</li>
                                <li>Delivery Schedule</li>
                                <li>Zone</li>
                                <li>Emirate</li>
                            </ul>
                        </div>
                        <div class="sec_2">
                            <ul class="sec_2_list">
                                <li>'.$orderDate.'</li>
                                <li>'.$orderdeliverydate.'</li>
                                <li>'.$orderdeliveryschedule.'</li>
                                <li>'.$location.'</li>
                                <li>'.$location.'</li>
                            </ul>
                        </div>
                    </div>
                    </div>
                    <div class="order_summary_bottom_right">
                    <p>Customer Details</p>
                    <div class="c_detail_content">
                    <div class="c_detail_content_left">
                    <h4>'.$customerName.'</h4>                
                    </div>
                    <div class="c_detail_content_right">
                    <h3>'.$customerlastName.'</h3>
                    </div>
                    </div>
                    </div>
                    </div>
                    
                </div>
            </div>';

             $ordersheet='<div class="order_sheet_body">
            <div class="order_sheet_body_sec_1">
                <p>SNo.</p>
                <div class="order_sheet_body_sec_1_content">
                '.$itemnumbersequence .'
                </div>
            </div>
            <div class="order_sheet_body_sec_2">
                <p>Item Description</p>
                <div class="order_sheet_body_sec_2_content">
                    <h3>FRUITS & VEGETABLES</h3>
                    '.$itemdescription.'
                </div>
            </div>
            <div class="order_sheet_body_sec_3">
                <p>Quantity</p>
                <div class="order_sheet_body_sec_3_content">
                   '.$qty.'
                </div>
            </div>
            </div>';

            $html2='
            <div class="order_sheet">
                '.$ordersheetcustomer.'
                '.$ordersheet.'
                </div>';

            $pdfhtml.= $html2;   
           
        }

        $html = '<!DOCTYPE html>
            <html>
            <head>
            <link rel="stylesheet" href="http://15.206.167.134/pub/media/bluethink/pdfdesign/pdf.css" type="text/css" media="all"/> </head>          
            <body><page size="A4">'.$pdfhtml.'</page></body>
            </html>';  
         /*echo $html;
         exit;*/

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        /*echo $storeManager->getStore()->getBaseUrl();
        exit;*/      

        //return $html;
            //$response = $this->dompdfFactory->create();           
            //$response->setFileName($incrementId);
           // $response->setData($html);
            file_put_contents("/var/www/html/pub/media/bluethink/pdfdesign/".$incrementId.".html", $html);
            shell_exec("wkhtmltopdf --collate /var/www/html/pub/media/bluethink/pdfdesign/".$incrementId.".html /var/www/html/pub/media/bluethink/pdfdesign/".$incrementId.".pdf");
           $filepath= "/var/www/html/pub/media/bluethink/pdfdesign/".$incrementId.".pdf";          
           $this->downloadpdf($filepath);
            shell_exec("rm /var/www/html/pub/media/bluethink/pdfdesign/".$incrementId.".pdf");
           // return $response;
            exit;
    }

   
    protected function massAction(AbstractCollection $collection)
    {

          return $this->getHtmlForPdf($collection);
         // exit;
            /*$response = $this->dompdfFactory->create();
            $response->setFileName('3000000017');
            $response->setData($this->getHtmlForPdf($collection));
            return $response;
            exit;*/
            /* $response = $this->dompdfFactory->create();
             $response->setFileName()
             $response->setData($this->getHtmlForPdf($collection));
            return $response;
            exit;*/
      /*  $countDeleteOrder = 0;
        $model = $this->_objectManager->create('Magento\Sales\Model\Order');
        foreach ($collection->getItems() as $order) {
            if (!$order->getEntityId()) {
                continue;
            }

             echo"************2<pre>";
             print_r($order->getEntityId());
             exit;
            $loadedOrder = $model->load($order->getEntityId());
            $loadedOrder->delete();
            $countDeleteOrder++;
        }
        $countNonDeleteOrder = $collection->count() - $countDeleteOrder;

        if ($countNonDeleteOrder && $countDeleteOrder) {
            $this->messageManager->addError(__('%1 order(s) cannot be done it .', $countNonDeleteOrder));
        } elseif ($countNonDeleteOrder) {
            $this->messageManager->addError(__('You done it  the order(s).'));
        }

        if ($countDeleteOrder) {
            $this->messageManager->addSuccess(__('We done it %1 order(s).', $countDeleteOrder));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->getComponentRefererUrl());
        return $resultRedirect;*/
    }

    public function downloadpdf($filepath)
    {

        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            exit;
        }
    }
}