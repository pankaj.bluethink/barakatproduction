<?php

/**
 * Product:       Xtento_PdfCustomizer
 * ID:            TGw4kxW+ydlBwD9IWqLRba7Ak6+QMl/CZVKgZOTCEUw=
 * Last Modified: 2019-02-05T17:13:45+00:00
 * File:          app/code/Xtento/PdfCustomizer/Block/Adminhtml/Tools.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\PdfCustomizer\Block\Adminhtml;

class Tools extends \Magento\Backend\Block\Template
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Xtento_PdfCustomizer::tools.phtml');
    }
}
