<?php

/**
 * Product:       Xtento_PdfCustomizer
 * ID:            TGw4kxW+ydlBwD9IWqLRba7Ak6+QMl/CZVKgZOTCEUw=
 * Last Modified: 2019-08-26T12:25:37+00:00
 * File:          app/code/Xtento/PdfCustomizer/Model/PdfTemplate.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\PdfCustomizer\Model;

use Xtento\PdfCustomizer\Api\Data\TemplatesInterface;
use Magento\Framework\Model\AbstractModel;

class PdfTemplate extends AbstractModel implements TemplatesInterface
{

    /**
     * Init resource model for the templates
     * @return void
     */
    public function _construct()
    {
        $this->_init('Xtento\PdfCustomizer\Model\ResourceModel\PdfTemplate');
    }

    public function getTemplateHtml()
    {
        // Change in version 2.7.9 from custom_ to formatted_ prefix to keep old templates compatible
        $originalTemplate = $this->getData('template_html');
        $adjustedTemplate = str_replace('var custom_', 'var formatted_', $originalTemplate);
        $adjustedTemplate = str_replace('var order.item.', 'var order_item.', $adjustedTemplate);
        $adjustedTemplate = str_replace('var formatted_item_if.', 'var item_if.', $adjustedTemplate);
        $adjustedTemplate = str_replace('var order.custom_item.', 'var formatted_order_item.', $adjustedTemplate);
        $adjustedTemplate = str_replace('var order_custom_item_product.', 'var formatted_order_item_product.', $adjustedTemplate);
        $adjustedTemplate = str_replace('var order_custom_item_product_if.', 'var order_item_product_if.', $adjustedTemplate);
        return $adjustedTemplate;
    }
}
