<?php

/**
 * Product:       Xtento_PdfCustomizer
 * ID:            TGw4kxW+ydlBwD9IWqLRba7Ak6+QMl/CZVKgZOTCEUw=
 * Last Modified: 2019-02-19T17:03:40+00:00
 * File:          app/code/Xtento/PdfCustomizer/Test/Unit/Model/PdfgeneratorRepositoryTest.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Xtento\PdfCustomizer\Test\Unit\Model;

use Xtento\PdfCustomizer\Model\PdfTemplateRepository;
use Xtento\PdfCustomizer\Model\ResourceModel\PdfTemplate as PdfTemplateResourceModel;
use Xtento\PdfCustomizer\Model\PdfTemplateFactory;
use Xtento\PdfCustomizer\Model\PdfTemplate as PdfTemplateModel;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Magento\Framework\Message\ManagerInterface;

/**
 * Test for \PdfTemplate\Model\PdfTemplateRepository
 * Class PdfTemplateRepositoryTest
 * @package Xtento\PdfCustomizer\Test\Integration
 */
class PdfTemplateRepositoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var /Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
     */
    public $objectManager;

    /**
     * @var PdfTemplateRepository
     */
    private $repository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Xtento\PdfCustomizer\Model\ResourceModel\PdfTemplate
     */
    private $pdfCustomizerResourceModel;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Xtento\PdfCustomizer\Model\PdfTemplateFactory
     */
    private $pdfCustomizerFactory;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Xtento\PdfCustomizer\Api\Data\TemplatesInterface;
     */
    private $pdfTemplate;

    public function setUp()
    {

        $this->objectManager = new ObjectManager($this);

        $this->pdfCustomizerResourceModel = $this->getMockBuilder(PdfTemplateResourceModel::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->pdfCustomizerFactory = $this->getMockBuilder(PdfTemplateFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        /** @var PdfTemplateModel pdfTemplate */
        $this->pdfTemplate = $this->objectManager->getObject(PdfTemplateModel::class);

        $this->pdfCustomizerFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->pdfTemplate);

        $messageManager = $this->getMockBuilder(ManagerInterface::class)->getMock();

        $this->repository = new PdfTemplateRepository(
            $this->pdfCustomizerResourceModel,
            $this->pdfTemplate,
            $this->pdfCustomizerFactory,
            $messageManager
        );
    }

    public function testSave()
    {
        $this->pdfCustomizerResourceModel
            ->expects($this->once())
            ->method('save')
            ->with($this->pdfTemplate)
            ->willReturnSelf();

        $this->assertEquals($this->pdfTemplate, $this->repository->save($this->pdfTemplate));
    }

    public function testGetById()
    {
        $id = 1;
        $this->pdfCustomizerResourceModel
            ->expects($this->once())
            ->method('load')
            ->with($this->pdfTemplate->setEntityId($id))
            ->willReturnSelf();

        $this->assertEquals($this->pdfTemplate, $this->repository->getById($id));
    }

    public function testDelete()
    {

        $this->pdfCustomizerResourceModel
            ->expects($this->once())
            ->method('delete')
            ->with($this->pdfTemplate)
            ->willReturnSelf();

        $this->assertTrue($this->repository->delete($this->pdfTemplate));
    }

    public function testDeleteById()
    {
        $id = 1;

        $this->pdfCustomizerResourceModel
            ->expects($this->once())
            ->method('load')
            ->with($this->pdfTemplate->setEntityId($id))
            ->willReturnSelf();

        $this->assertTrue($this->repository->deleteById($id));
    }
}
