<?php

namespace PayUIndia\Payu\Model;

use Magento\Sales\Api\Data\TransactionInterface;

class Payu extends \Magento\Payment\Model\Method\AbstractMethod {

    const PAYMENT_PAYU_CODE = 'payu';
    const ACC_BIZ = 'payubiz';
    const ACC_MONEY = 'payumoney';

    protected $_code = self::PAYMENT_PAYU_CODE;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;
    protected $_supportedCurrencyCodes = array(
        'AFN', 'ALL', 'DZD', 'ARS', 'AUD', 'AZN', 'BSD', 'BDT', 'BBD',
        'BZD', 'BMD', 'BOB', 'BWP', 'BRL', 'GBP', 'BND', 'BGN', 'CAD',
        'CLP', 'CNY', 'COP', 'CRC', 'HRK', 'CZK', 'DKK', 'DOP', 'XCD',
        'EGP', 'EUR', 'FJD', 'GTQ', 'HKD', 'HNL', 'HUF', 'INR', 'IDR',
        'ILS', 'JMD', 'JPY', 'KZT', 'KES', 'LAK', 'MMK', 'LBP', 'LRD',
        'MOP', 'MYR', 'MVR', 'MRO', 'MUR', 'MXN', 'MAD', 'NPR', 'TWD',
        'NZD', 'NIO', 'NOK', 'PKR', 'PGK', 'PEN', 'PHP', 'PLN', 'QAR',
        'RON', 'RUB', 'WST', 'SAR', 'SCR', 'SGF', 'SBD', 'ZAR', 'KRW',
        'LKR', 'SEK', 'CHF', 'SYP', 'THB', 'TOP', 'TTD', 'TRY', 'UAH',
        'AED', 'USD', 'VUV', 'VND', 'XOF', 'YER'
    );
    private $checkoutSession;
    /**
     * 
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
      public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \PayUIndia\Payu\Helper\Payu $helper,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Checkout\Model\Session $checkoutSession      
              
    ) {
        $this->helper = $helper;
        $this->orderSender = $orderSender;
        $this->httpClientFactory = $httpClientFactory;
        $this->checkoutSession = $checkoutSession;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger
        );

    }

    public function canUseForCurrency($currencyCode) {
        if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
            return false;
        }
        return true;
    }

    public function getRedirectUrl() {
        return $this->helper->getUrl($this->getConfigData('redirect_url'));
    }

    public function getReturnUrl() {
        return $this->helper->getUrl($this->getConfigData('return_url'));
    }

    public function getCancelUrl() {
        return $this->helper->getUrl($this->getConfigData('cancel_url'));
    }

    /**
     * Return url according to environment
     * @return string
     */
    public function getCgiUrl() {
        $env = $this->getConfigData('environment');
        if ($env === 'production') {
            return $this->getConfigData('production_url');
        }
        return $this->getConfigData('sandbox_url');
    }
    public function buildCheckoutRequest() {
         $order = $this->checkoutSession->getLastRealOrder();
        $billing_address = $order->getBillingAddress();

         $currency ="AED";
        $returnpath ="https://partner.ctdev.comtrust.ae/banktestnbad/Authorization.aspx?capture=true";
        $transactionhint ="CPT:Y";
        $orderid   = $this->checkoutSession->getLastRealOrderId();
        $channel = "Web";
        $customer = "Demo Merchant";
        $amount  =  round($order->getBaseGrandTotal(), 2);
        $ordername ="Online Shopping BARAKAT";
        $username  = $this->getConfigData("merchant_key");
        $password = $this->getConfigData('salt');

        $value = array('Registration' =>
            array(
                'Currency' => $currency,
                'ReturnPath' => $returnpath,
                'TransactionHint' => $transactionhint,
                'OrderID' => $orderid,
                'Channel' => $channel,
                'Amount' => $amount,
                'Customer' => $customer,
                'OrderName' => $ordername,
                'UserName' => $username,
                'Password' => $password,
            ) );
     
        $response = $this->curlRequest($value);
        $data = json_decode($response,true);
        $params["TransactionID"] = $data['Transaction']['TransactionID']; 
        
        return $params;
    }


 public function curlRequest($value){

      
 $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_PORT => "2443",
  CURLOPT_URL => "https://demo-ipg.ctdev.comtrust.ae:2443/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => json_encode($value),
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 5533d115-2773-c836-eb5c-81a32d1351ee"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
return $response ;

    }
   



   /* public function generatePayuHash($txnid, $amount, $productInfo, $name, $email) {
        $SALT = $this->getConfigData('salt');

        $posted = array(
            'key' => $this->getConfigData("merchant_key"),
            'txnid' => $txnid,
            'amount' => $amount,
            'productinfo' => $productInfo,
            'firstname' => $name,
            'email' => $email,
        );

        $hashSequence = 'key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10';

        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';
        foreach ($hashVarsSeq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }
        $hash_string .= $SALT;
        return strtolower(hash('sha512', $hash_string));
    }*/

    //validate response
    /*public function validateResponse($returnParams) {
		$order = $this->checkoutSession->getLastRealOrder();
		$SALT = $this->getConfigData('salt');
		
		$key = $returnParams['key'];
		$txnid = $this->checkoutSession->getLastRealOrderId();
		$amount = number_format($order->getBaseGrandTotal(), 2);
		$productInfo = 	$returnParams['productinfo'];
		$firstname    		= 	$returnParams['firstname'];
		$email        		=	$returnParams['email'];
		$keyString 	  		=  	$key.'|'.$txnid.'|'.$amount.'|'.$productInfo.'|'.$firstname.'|'.$email.'||||||||||';
		$keyArray 	  		= 	explode("|",$keyString);
		$reverseKeyArray 	= 	array_reverse($keyArray);
		$reverseKeyString	=	implode("|",$reverseKeyArray);
		
		if ((isset($returnParams['status']) && $returnParams['status'] == 'success')  && ($txnid == $returnParams['txnid'])) {
			$saltString     = $SALT.'|'.$returnParams['status'].'|'.$reverseKeyString;
			$sentHashString = strtolower(hash('sha512', $saltString));
			
			if(($sentHashString==$returnParams['hash']) && ($amount == $returnParams['amount'])){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
		
        /* if ($returnParams['status'] == 'pending' || $returnParams['status'] == 'failure') {
            return false;
        }
        if ($returnParams['key'] != $this->getConfigData("merchant_key")) {
            return false;
        } */
		
		/*return true;
    }*/

   /* public function postProcessing(\Magento\Sales\Model\Order $order,
            \Magento\Framework\DataObject $payment, $response) {
        
        $payment->setTransactionId($response['txnid']);
        $payment->setTransactionAdditionalInfo('payu_mihpayid',
                $response['mihpayid']);
        $payment->setAdditionalInformation('payu_order_status', 'approved');
        $payment->addTransaction(TransactionInterface::TYPE_ORDER);
        $payment->setIsTransactionClosed(0);
        $payment->place();
        $order->setStatus('processing');
        $order->save();
    }*/
}
