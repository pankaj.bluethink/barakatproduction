<?php
/**
 * @package Order_Status
 * @author Sachin Gupta <sachin.bluethink@gmail.com>
 */

namespace Delivery\Management\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;

class Edit extends \Magento\Backend\Block\Template
{
    public function getFormAction()
    {
        return $this->getUrl('delivery_management/*/getorder', ['_current' => true]);
    }

    public function getAjaxData()
    {
        return $this->getData();
    }

    public function getPicker()
    {
   
        return [
            ['value' => '', 'label' => __('-- Please Select --')],
            ['value' => 'Raj', 'label' => __('Raj')],
            ['value' => 'Faisal', 'label' => __('Faisal')],
             ['value' => 'Umair', 'label' => __('Umair')],
            ['value' => 'Renato', 'label' => __('Renato')],
             ['value' => 'Jobert', 'label' => __('Jobert')],
            ['value' => 'Hassan', 'label' => __('Hassan')],
             ['value' => 'Ibrahim', 'label' => __('Ibrahim')],
            ['value' => 'Amjad', 'label' => __('Amjad')],
             ['value' => 'Walia', 'label' => __('Walia')],
            ['value' => 'Yasir', 'label' => __('Yasir')]
        ];
    }

    public function getOrderUpdateAction($incrementId)
    {
        return $this->getUrl('delivery_management/*/updateorder', ['_current' => true, 'incrementId' => $incrementId]);
    }

}   
