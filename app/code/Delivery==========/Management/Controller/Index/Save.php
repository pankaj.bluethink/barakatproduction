<?php
/**
 * @category   Bluethink
 * @package    Bluethink_Bulkorder
 * @author     vishal.bluethink@gmail.com

 */

namespace Delivery\Management\Controller\Index;

use Magento\Framework\App\Action\Context;
use Delivery\Management\Model\ManagementFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;

class Save extends \Magento\Framework\App\Action\Action
{
	/**
     * @var Bulkorder
     */
    protected $management;
    protected $uploaderFactory;
    protected $adapterFactory;
    protected $filesystem;

    public function __construct(
		Context $context,
        ManagementFactory $management,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem
    ) {
        $this->_management = $management;
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }
	public function execute()
    {    
           
     
        $data = $this->getRequest()->getParams();
        if(isset($data['_order_id']) && $data['_order_id'] != '')
        {   
            $checkbulkorder = $this->_management->create()->getCollection();
            $checkbulkorder->addFieldToFilter('_order_id',$data['_order_id']);

            $bulkorder = $this->_management->create();
            if(count($checkbulkorder->getData()) >0)
            { 
                $bulkorder->load($checkbulkorder->getData()[0]['id']);
                $bulkorder->setId($checkbulkorder->getData()[0]['id']);
            }
            
            $bulkorder->addData($data);
            if($bulkorder->save()){
                $this->messageManager->addSuccessMessage(__('Your bulk order data saved.'));
            }else{
                $this->messageManager->addErrorMessage(__('bulk order Data was not saved.'));
            }
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('management/index/list');
            return $resultRedirect;    

        }
        
        }
        
    	
    
}
