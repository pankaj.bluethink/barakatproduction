<?php
/**
 * @category   Order
 * @package    Order_Status
 * @author     Sachin Gupta <sachin.bluethink@gmail.com>
 */

namespace Delivery\Management\Controller\Adminhtml\Items;

class Delivered extends \Delivery\Management\Controller\Adminhtml\Items
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
 
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Delivery_Management::Delivered');
        $resultPage->getConfig()->getTitle()->prepend(__('Delivered'));
        return $resultPage;
    }
}