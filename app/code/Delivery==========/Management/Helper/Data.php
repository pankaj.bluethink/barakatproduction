<?php

namespace Delivery\Management\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	// replace by constant value   
    const PROCESS_STATUS = "process_pending";

    const READY_TO_DISPATCH = "ready_to_dispatch";

    const DELIVERY_IN_PROGRESS = "delivery_in_progress";

    const DELIVERED = "delivered";



public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => __('-- Please Select --')],
            ['value' => 'Raj', 'label' => __('Raj')],
            ['value' => 'Zeayul', 'label' => __('Zeayul')]
        ];
    }
}

