<?php
/**
 * @package Order_Status
 * @author Sachin Gupta <sachin.bluethink@gmail.com>
 */

namespace Delivery\Management\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;

class Edit extends \Magento\Backend\Block\Template
{
    public function getFormAction()
    {
        return $this->getUrl('delivery_management/*/getorder', ['_current' => true]);
    }

    public function getAjaxData()
    {
        return $this->getData();
    }

    public function getPicker()
    {
   
        return [
            ['value' => 'Ashrtaf', 'label' => 'Ashrtaf'],
            ['value' => 'Tek', 'label' => 'Tek']
        ];
    }

    public function getOrderUpdateAction($incrementId)
    {
        return $this->getUrl('delivery_management/*/updateorder', ['_current' => true, 'incrementId' => $incrementId]);
    }

}   
