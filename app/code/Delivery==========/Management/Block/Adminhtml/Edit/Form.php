<?php
/**
 * @package Order_Status
 * @author Sachin Gupta <sachin.bluethink@gmail.com>
 */

namespace Delivery\Management\Block\Adminhtml\Edit;

use Magento\Backend\Block\Template\Context;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory
    )
    {
        parent::__construct($context, $registry, $formFactory);
    }
    /**
     * Return form action url
     *
     * @return string
     */
    public function getActionUrl()
    {
        return $this->getUrl('adminhtml/*/save', ['_current' => true]);
    }

    protected function _prepareForm()
    { //die("hhh");
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            array(
                'data' => array(
                    'id' => 'edit_form',
                    'action' => $this->getActionUrl(),
                    'method' => 'post'
                )
            )
        );
        
        $fieldset = $form->addFieldset(
            'general_fieldset',
            ['legend' => __('Get Order Details'), 'class' => 'fieldset-wide']
        );

        $this->_addElementTypes($fieldset);

        $fieldset->addField(
            'order_id',
            'text',
            [
                'label'     => __('Scan Barcode/ Enter Order Ref.'),
                'title'     => __('Scan Barcode/ Enter Order Ref.'),
                'name'      => 'order_id',
                'note'      => __('<small>Enter the Order Id</small>'),
                'class'    => 'fieldset-small',
                'required'  => true
            ]
        );

        $fieldset->addField('submit', 'submit', array(
            'label'     => Mage::helper('form')->__('Submit'),
          'required'  => true,
          'value'  => 'Submit',
          'after_element_html' => '<small>Comments</small>',
          'tabindex' => 1
        ));

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
