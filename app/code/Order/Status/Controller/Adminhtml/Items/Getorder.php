<?php
/**
 *  @package Order_Status
 *  @author Sachin Gupta <sachin.bluethnik@gmail.com>
 */

namespace Order\Status\Controller\Adminhtml\Items;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;

class Getorder extends \Magento\Backend\App\Action
{
    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    protected $resultJsonFactory;

    protected $_orderInterface;

    protected $orderFactory;

    protected $_orderCollectionFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Sales\Api\Data\OrderInterfaceFactory $orderFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->orderFactory = $orderFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
    }
	
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $resultPage = $this->resultPageFactory->create();
        $data = $this->getRequest()->getPostValue();
        $orderId = $data['orderId'];
        $ajaxResponse = [];
        if ($orderId) {
            //$order = $this->orderFactory->create()->loadByIncrementId($orderId);
            $orderData = $this->_orderCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('status',['in' => \Order\Status\Helper\Data::PROCESS_STATUS])
                    ->addFieldToFilter('increment_id', $orderId);
            if (!empty($orderData)) {
                foreach ($orderData as $order) {
                    /*echo "<pre>@@@@";
                    print_r($order->getData());
                    exit;*/
                    $ajaxResponse['status']                 = $order->getStatus();
                    $ajaxResponse['order_id']               = $order->getEntityId();
                    $ajaxResponse['increment_id']           = $order->getIncrementId();
                    $ajaxResponse['customer_id']            = $order->getCustomerId();
                    $ajaxResponse['customer_email']         = $order->getCustomerEmail();
                    $ajaxResponse['customer_first_name']    = $order->getCustomerFirstname();
                    $ajaxResponse['customer_last_name']     = $order->getCustomerLastname();
                    $ajaxResponse['billing_address']           = array(
                        'customer_address_id' => $order->getBillingAddress()->getCustomerAddressId(),
                        'street' => $order->getBillingAddress()->getStreet(),
                        'city'  => $order->getBillingAddress()->getCity(),
                        'region' => $order->getBillingAddress()->getRegion(),
                        'post_code' => $order->getBillingAddress()->getPostCode(),
                        'country_id' => $order->getBillingAddress()->getCountryId(),
                        'telephone' => $order->getBillingAddress()->getTelephone()
                        );

                    $ajaxResponse['shipping_address']           = array(
                        'customer_address_id' => $order->getShippingAddress()->getCustomerAddressId(),
                        'street' => $order->getShippingAddress()->getStreet(),
                        'city'  => $order->getShippingAddress()->getCity(),
                        'region' => $order->getShippingAddress()->getRegion(),
                        'post_code' => $order->getShippingAddress()->getPostCode(),
                        'country_id' => $order->getShippingAddress()->getCountryId(),
                        'telephone' => $order->getBillingAddress()->getTelephone(),
                        );

                    $i = 0;
                    foreach ($order->getAllVisibleItems() as $item)
                    {
                        $ajaxResponse['product'][$i] = array(
                            'product_id' => $item->getId(),
                            'product_name' => $item->getSku(),
                            'ordered_qty' => $item->getQtyOrdered(),
                            'price' => $item->getPrice()
                        );
                        $i++;
                    }

                    $ajaxResponse['grand_total'] = $order->getGrandTotal();
                    $ajaxResponse['order_delivery_date'] = $order->getOrderDeliveryDate();
                    $ajaxResponse['order_delivery_time'] = $order->getOrderDeliveryTime();
                }

                /*echo "<pre>@@@";
                print_r($ajaxResponse);
                exit;*/
            }
            $block = $resultPage->getLayout()
                ->createBlock('Order\Status\Block\Adminhtml\Edit')
                ->setTemplate('Order_Status::result.phtml')
                ->setData('data',$ajaxResponse)
                ->toHtml();

            $result->setData([
                'status' => 1,
                'output' => $block
            ]);
            return $result;
        }else{
            $result->setData([
                'status' => false,
                'output' => ''
            ]);
        }
    }
}

?>