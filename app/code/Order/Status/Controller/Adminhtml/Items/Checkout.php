<?php
/**
 * @category   Order
 * @package    Order_Status
 * @author     Sachin Gupta <sachin.bluethink@gmail.com>
 */

namespace Order\Status\Controller\Adminhtml\Items;

class Checkout extends \Order\Status\Controller\Adminhtml\Items
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Order_Status::Order');
        $resultPage->getConfig()->getTitle()->prepend(__('Checkout Process'));
        return $resultPage;
    }
}