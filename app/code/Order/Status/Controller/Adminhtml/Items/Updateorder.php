<?php
/**
 *  @package Order_Status
 *  @author Sachin Gupta <sachin.bluethnik@gmail.com>
 */

namespace Order\Status\Controller\Adminhtml\Items;

use \Magento\Sales\Api\OrderRepositoryInterface;

class Updateorder extends \Magento\Backend\App\Action
{
    protected $_orderRepository;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        OrderRepositoryInterface $orderRepository
    )
    {
        parent::__construct($context);
        $this->_orderRepository = $orderRepository;
    }
	
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $orderId = $this->getRequest()->getParam('incrementId');
        $order = $this->_orderRepository->get($orderId);
        $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
        $order->setStatus(\Order\Status\Helper\Data::READY_TO_DISPATCH);
        $order->setDeliveryPerson($this->getRequest()->getPostValue('delivery_person'));
        $order->addStatusToHistory($order->getStatus(), 'Order processed successfully and now ready for dispatch');

        try {
            if ($this->_orderRepository->save($order)) {
                $this->messageManager->addSuccess(
                    __('Order is Succssfully added Ready to Dispatch')
                );
                return $resultRedirect->setPath('order_status/*/checkout', ['_current' => true]);
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        }
    }
}

?>