<?php

namespace Order\Status\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	// replace by constant value   
    const PROCESS_STATUS = "process_pending";

    const READY_TO_DISPATCH = "ready_to_dispatch";

    const DELIVERY_IN_PROGRESS = "delivery_in_progress";

    const DELIVERED = "delivered";



public function toOptionArray()
    {
        return [
            ['value' => 'Ashrtaf', 'label' => 'Ashrtaf'],
            ['value' => 'Tek', 'label' => 'Tek'],
            ['value' => 'Abhishek Kalia', 'label' => 'Abhishek Kalia'],
            ['value' => 'Bhabindra', 'label' => 'Bhabindra'],
            ['value' => 'Bhim', 'label' => 'Bhim'],
            ['value' => 'Irshad', 'label' => 'Irshad'],
            ['value' => 'Nashan', 'label' => 'Nashan'],
            ['value' => 'Suraj', 'label' => 'Suraj'],
            ['value' => 'Unni', 'label' => 'Unni'],
            ['value' => 'Vipin', 'label' => 'Vipin'],
            ['value' => 'Jamshed', 'label' => 'Jamshed'],
            ['value' => 'NA', 'label' => 'NA']
        ];
    }
}


