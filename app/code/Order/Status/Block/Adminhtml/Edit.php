<?php
/**
 * @package Order_Status
 * @author Sachin Gupta <sachin.bluethink@gmail.com>
 */

namespace Order\Status\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;

class Edit extends \Magento\Backend\Block\Template
{
    public function getFormAction()
    {
        return $this->getUrl('order_status/*/getorder', ['_current' => true]);
    }

    public function getAjaxData()
    {
        return $this->getData();
    }

    public function getPicker()
    {
   
        return [
            ['value' => 'Ashrtaf', 'label' => 'Ashrtaf'],
            ['value' => 'Tek', 'label' => 'Tek'],
            ['value' => 'Abhishek Kalia', 'label' => 'Abhishek Kalia'],
            ['value' => 'Bhabindra', 'label' => 'Bhabindra'],
            ['value' => 'Bhim', 'label' => 'Bhim'],
            ['value' => 'Irshad', 'label' => 'Irshad'],
            ['value' => 'Nashan', 'label' => 'Nashan'],
            ['value' => 'Suraj', 'label' => 'Suraj'],
            ['value' => 'Unni', 'label' => 'Unni'],
            ['value' => 'Vipin', 'label' => 'Vipin'],
            ['value' => 'Jamshed', 'label' => 'Jamshed'],
            ['value' => 'NA', 'label' => 'NA']
        ];
    }

    public function getOrderUpdateAction($incrementId)
    {
        return $this->getUrl('order_status/*/updateorder', ['_current' => true, 'incrementId' => $incrementId]);
    }

}   
