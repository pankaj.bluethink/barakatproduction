<?php
namespace Linchpin\AdvancedSorting\Plugin\Model;

use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Config
 * @package Linchpoin\AdvancedSorting\Plugin\Model
 */
class Config  {

	/**
	 * @var StoreManagerInterface
	 */
	protected $_storeManager;

	/**
	 * Config constructor.
	 * @param StoreManagerInterface $storeManager
	 *
	 */
	public function __construct(
		StoreManagerInterface $storeManager
	) {
		$this->_storeManager = $storeManager;
	}

	/**
	 * Adding custom options and changing labels
	 *
	 * @param \Magento\Catalog\Model\Config $catalogConfig
	 * @param [] $options
	 * @return []
	 */
	public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
	{
		$store = $this->_storeManager->getStore();
		$currencySymbol = $store->getCurrentCurrency()->getCurrencySymbol();

		// Remove specific default sorting options
		$default_options = [];
		$default_options['name'] = $options['name'];

		unset($options['position']);
		unset($options['name']);
		unset($options['price']);
	
		unset($options['name_asc']);



		//Changing label
		$customOption['position'] = __( 'Filter By' );

		//New sorting options name~asc top_rated~desc
		$customOption['price_desc'] = __( $currencySymbol . 'Price High to Low' );
		$customOption['price_asc'] = __( $currencySymbol . 'Price Low to High' );
		/*$customOption['name_asc'] = __( $currencySymbol . 'Alphabetically' );*/
		
		/*$customOption['name'] = $default_options['name'];*/

		//Merge default sorting options with custom options
		$options = array_merge($customOption, $options);

		return $options;
	}
}