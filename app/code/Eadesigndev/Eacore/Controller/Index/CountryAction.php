<?php

namespace Eadesigndev\Eacore\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Directory\Helper\Data;
use Eadesigndev\RomCity\Model\RomCityRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;

class CountryAction extends Action
{
   protected $_pageFactory;

   protected $_regCollectionFactory;

   protected $_helperData;

   private $searchCriteria;

   private $romCityRepository;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regCollectionFactory,
        Data $helperData,
        SearchCriteriaBuilder $searchCriteria,
        RomCityRepository $romCityRepository
    ) {
        $this->_regCollectionFactory = $regCollectionFactory;
        $this->_pageFactory = $pageFactory;
        $this->_helperData = $helperData;
        $this->searchCriteria = $searchCriteria;
         $this->romCityRepository = $romCityRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $countryIds = ['AE'];
        $cities='';
        $collection = $this->_regCollectionFactory->create();
        $collection->addCountryFilter($countryIds)->load();
        $regions = [
            'config' => [
                'show_all_regions' => $this->_helperData->isShowNonRequiredState(),
                'regions_required' => $this->_helperData->getCountriesWithStatesRequired(),
            ],
        ];

        $searchCriteriaBuilder = $this->searchCriteria;
        $searchCriteria = $searchCriteriaBuilder->create();

        $citiesList = $this->romCityRepository->getList($searchCriteria);
        $items = $citiesList->getItems();

        /** @var RomCity $item */
        foreach ($items as $item) {
            $citiesData[$item->getEntityId()] = $item;
        }

        foreach ($collection as $region) {
        	
            /** @var $region \Magento\Directory\Model\Region */
            if (!$region->getRegionId()) {
                continue;
            }
             if ($region->getRegionId() != $this->getRequest()->getPost('cityid')) {
                continue;
            }
            foreach ($citiesData as $cityId => $cityData) {
                $entityId = $cityData->getRegionId();
                $regionId = $region->getId();
                if ($entityId == $regionId) {
                    $id       = $cityData->getId();
                    $cities.= '<option value="'.$cityData->getCityName().'"">'.$cityData->getCityName().'</option>';
                    // $cities[$id] = [
                    //     'name' => $cityName,
                    //     'id' => $cityId
                    // ];
                }
            }
            // echo "<pre>++++";
            // print_r($cities);
           
            // $regions[$region->getCountryId()][$region->getRegionId()] = [
            //     'code' => $region->getCode(),
            //     'name' => (string)__($region->getName()),
            //     'cities' => $cities
            // ];
        }

        // echo "<pre>@@@";
        // print_r($regions);
        // exit;
        // return $regions;
        echo $cities;
     exit;
             //return $this->_pageFactory->create();
    }
}