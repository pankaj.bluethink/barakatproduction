<?php 

namespace Eadesigndev\RomCity\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;

class SalesOrderPlaceAfter implements ObserverInterface
{
   protected $_orderRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository
    )
    {
        $this->_orderRepository = $orderRepository;
    }
 
 public function execute(\Magento\Framework\Event\Observer $observer)
 {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $logger = $objectManager->create('\Psr\Log\LoggerInterface');   
    $order = $observer->getEvent()->getOrder();
    $logger->debug("Updating the order city and region for the order id :". $order->getEntityId());
    $shippingAddress = $order->getShippingAddress();
    $order->setZoneArea($shippingAddress->getCity());
    $order->setLocation($shippingAddress->getRegion());
    try {
       $order->save();
    } catch (\Exception $e) {
        throw new Exception("Error Processing Request", 1);
        
    }

    }
}














?>