<?php
/**
 * @category   Vendor
 * @package    Vendor_Extension
 * @author     shashank.bluethink@gmail.com
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Vendor\Extension\Block;

use Magento\Framework\View\Element\Template\Context;
use Vendor\Extension\Model\ExtensionFactory;
use Magento\Cms\Model\Template\FilterProvider;
/**
 * Extension View block
 */
class ExtensionView extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Extension
     */
    protected $_extension;
    public function __construct(
        Context $context,
        ExtensionFactory $extension,
        FilterProvider $filterProvider
    ) {
        $this->_extension = $extension;
        $this->_filterProvider = $filterProvider;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Vendor Extension Module View Page'));
        
        return parent::_prepareLayout();
    }

    public function getSingleData()
    {
        $id = $this->getRequest()->getParam('id');
        $extension = $this->_extension->create();
        $singleData = $extension->load($id);
        if($singleData->getExtensionId() && $singleData->getStatus() == 1){
            return $singleData;
        }else{
            return false;
        }
    }
}