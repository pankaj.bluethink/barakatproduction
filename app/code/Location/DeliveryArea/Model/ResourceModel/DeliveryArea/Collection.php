<?php
/**
 * @category   Location
 * @package    Location_DeliveryArea
 * @author     shashank.bluethink@gmail.com
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Location\DeliveryArea\Model\ResourceModel\DeliveryArea;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'deliveryarea_id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Location\DeliveryArea\Model\DeliveryArea',
            'Location\DeliveryArea\Model\ResourceModel\DeliveryArea'
        );
    }
}