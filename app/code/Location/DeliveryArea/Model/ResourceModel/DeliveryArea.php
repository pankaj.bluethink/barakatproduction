<?php
/**
 * @category   Location
 * @package    Location_DeliveryArea
 * @author     shashank.bluethink@gmail.com
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Location\DeliveryArea\Model\ResourceModel;

class DeliveryArea extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('location_deliveryarea', 'deliveryarea_id');   //here "location_deliveryarea" is table name and "deliveryarea_id" is the primary key of custom table
    }
}