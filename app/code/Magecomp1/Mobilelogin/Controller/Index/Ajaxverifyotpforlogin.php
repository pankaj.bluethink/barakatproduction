<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;

class Ajaxverifyotpforlogin extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;
    protected $session;
    protected $cookieMetadataManager;
    protected $scopeConfig;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        \Magecomp\Mobilelogin\Helper\Data $helperData,
        Session $customerSession,
        CookieMetadataFactory $cookieMetadataFactory,
        AccountRedirect $accountRedirect,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->session = $customerSession;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->accountRedirect = $accountRedirect;
        $this->session = $customerSession;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }
    public function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->
            get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    public function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

    public function execute()
    {
        $data = "false";
        $mobile = $this->getRequest()->get('mobile');
        $otp = $this->getRequest()->get('otp');
        $isExist = $this->_helperdata->checkLoginOTPCode($mobile, $otp);
        if ($isExist == 1) {
            //$customerData = $this->_objectManager->create('\Magento\Customer\Model\Customer');
            // $customer = $customerData->getCollection()->addFieldToFilter("mobilenumber", $mobile)->getFirstItem();
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $customerData = $om->create('\Magento\Customer\Model\Customer');
            $customer = $customerData->getCollection()->addFieldToFilter("mobilenumber", $mobile)->getFirstItem();
            if ($customer) {
                $this->session->setCustomerAsLoggedIn($customer);
                $this->session->regenerateId();
                $data = "true";

                if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                    $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                    $metadata->setPath('/');
                    $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                }
                $redirectUrl = $this->accountRedirect->getRedirectCookie();
                if (!$this->scopeConfig->getValue('customer/startup/redirect_dashboard') && $redirectUrl) {
                    $this->accountRedirect->clearRedirectCookie();
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setUrl($this->_redirect->success($redirectUrl));
                    //return $resultRedirect;
                    $data="true";
                }

                if ($this->_helperdata->isEnableLoginEmail()) {
                    $this->_helperdata->sendMail($_SERVER['REMOTE_ADDR'], $customer->getEmail(), $_SERVER['HTTP_USER_AGENT']);
                }
            }
        }
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);
        return $resultJson;
    }
}