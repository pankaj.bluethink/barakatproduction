<?php
namespace Magecomp\Mobilelogin\Helper;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Exception\LocalizedException;

class Apicall extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $_storeManager;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;

    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/moduleoption/enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getAuthkey()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/authkey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getRouttype()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/routtype',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getPassword()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/password',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getUsername()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/username',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiUrl()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/apiurl',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getSenderId()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/senderid',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function curlApiCall($message,$mobilenumbers)
    {
        if($this->isEnable())
        {
            $postData = array(
                'user' => $this->getUsername(),
                'pwd' => $this->getPassword(),
                'senderid' => $this->getSenderId(),
                'mobileno'=>$mobilenumbers,
                'msgtext' => $message,
                'CountryCode'=>'ALL'
            );
            $ch = curl_init();
            if (!$ch)
            {
                throw new LocalizedException("Couldn't initialize a cURL handle");
            }

            $ret = curl_setopt($ch, CURLOPT_URL,$this->getApiUrl());
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt ($ch, CURLOPT_POSTFIELDS,$postData);

            $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $curlresponse = curl_exec($ch); // execute
            
            if(curl_errno($ch))
                return "error";

            if (empty($ret))
            {
                curl_close($ch); // close cURL handler
                return "error";
            }
            else
            {
                curl_close($ch); // close cURL handler
            }
            return "true";
        }
        else
        {
            return "false";
        }
    }
}