<?php

namespace BQP\Order\Model\ResourceModel\Processing;

class Collection extends \Magento\Sales\Model\ResourceModel\Order\Grid\Collection
{
    
    public function getCurrentPage(){
        return $this->getCurPage();
    }

    public function setCurrentPage($currentPage)
    {
        return $this->setCurPage($currentPage);
    }
}
