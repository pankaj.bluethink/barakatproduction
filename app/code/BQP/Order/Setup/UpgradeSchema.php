<?php 

namespace BQP\Order\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'complementary_items',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 20,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'complementary items'
               ]
           );
           $connection->addColumn(
            $installer->getTable('sales_order_item'),
               'item_organisation',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 30,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'item_organisation'
               ]
           ); 
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_grid'),
               'delivery_person',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 20,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'delivery_person'
               ]
           );
            $connection->addColumn(
            $installer->getTable('sales_order'),
               'delivery_person',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 20,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'delivery_person'
               ]
           );
          
        }
         if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_grid'),
               'delivery_picker',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 20,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'delivery_picker'
               ]
           );
            $connection->addColumn(
            $installer->getTable('sales_order'),
               'delivery_picker',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 20,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'delivery_picker'
               ]
           );
          
        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $connection->addColumn(
            $installer->getTable('sales_order_grid'),
               'zone_area',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 20,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'zone area'
               ]
           );
            $connection->addColumn(
            $installer->getTable('sales_order'),
               'zone_area',
               [
                   'type' => Table::TYPE_TEXT,
                   'length' => 20,
                   'nullable' => true,
                   'default' => NULL,
                   'comment' => 'zone area'
               ]
           );
          
        }
         $installer->endSetup();
    }
}