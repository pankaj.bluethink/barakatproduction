<?php

namespace BQP\Order\Plugin;

use Magento\Framework\Serialize\SerializerInterface;


class QuoteItemToOrderItemPlugin
{
    public $product;
    public function __construct(
        \Magento\Catalog\Model\Product $product
        
    ) {
        $this->product = $product;
    }

    public function aroundConvert(\Magento\Quote\Model\Quote\Item\ToOrderItem $subject, callable $proceed, $quoteItem, $data)
    {
        $product_id = $quoteItem->getProduct()->getId();    
        $item_org = $this->product->load($product_id)->getItemOrganisation();
        $orderItem = $proceed($quoteItem, $data);
        $orderItem->setItemOrganisation($item_org);
        return $orderItem;
    }
}